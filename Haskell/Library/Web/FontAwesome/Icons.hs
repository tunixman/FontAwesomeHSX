{-# OPTIONS_GHC -fno-warn-warnings-deprecations #-}
module Web.FontAwesome.Icons where

import Web.FontAwesome.Types
import Data.Set (fromList)
import Data.Version

fa_glass :: Icon
fa_glass = Icon {_icoName = IconName {_icnName = "Glass"}, _icoId = IconId {_iidId = "glass"}, _icoChar = Unicode {_uniChar = '\61440'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "alcohol"},Filter {_filtFilter = "bar"},Filter {_filtFilter = "drink"},Filter {_filtFilter = "liquor"},Filter {_filtFilter = "martini"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_music :: Icon
fa_music = Icon {_icoName = IconName {_icnName = "Music"}, _icoId = IconId {_iidId = "music"}, _icoChar = Unicode {_uniChar = '\61441'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "note"},Filter {_filtFilter = "sound"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_search :: Icon
fa_search = Icon {_icoName = IconName {_icnName = "Search"}, _icoId = IconId {_iidId = "search"}, _icoChar = Unicode {_uniChar = '\61442'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "bigger"},Filter {_filtFilter = "enlarge"},Filter {_filtFilter = "magnify"},Filter {_filtFilter = "zoom"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_envelope_o :: Icon
fa_envelope_o = Icon {_icoName = IconName {_icnName = "Envelope Outlined"}, _icoId = IconId {_iidId = "envelope-o"}, _icoChar = Unicode {_uniChar = '\61443'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "e-mail"},Filter {_filtFilter = "email"},Filter {_filtFilter = "letter"},Filter {_filtFilter = "mail"},Filter {_filtFilter = "notification"},Filter {_filtFilter = "support"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_heart :: Icon
fa_heart = Icon {_icoName = IconName {_icnName = "Heart"}, _icoId = IconId {_iidId = "heart"}, _icoChar = Unicode {_uniChar = '\61444'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "favorite"},Filter {_filtFilter = "like"},Filter {_filtFilter = "love"}]), _icoCategories = fromList [Category {_catCategory = "Medical Icons"},Category {_catCategory = "Web Application Icons"}]}

fa_star :: Icon
fa_star = Icon {_icoName = IconName {_icnName = "Star"}, _icoId = IconId {_iidId = "star"}, _icoChar = Unicode {_uniChar = '\61445'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "achievement"},Filter {_filtFilter = "award"},Filter {_filtFilter = "favorite"},Filter {_filtFilter = "night"},Filter {_filtFilter = "rating"},Filter {_filtFilter = "score"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_star_o :: Icon
fa_star_o = Icon {_icoName = IconName {_icnName = "Star Outlined"}, _icoId = IconId {_iidId = "star-o"}, _icoChar = Unicode {_uniChar = '\61446'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "achievement"},Filter {_filtFilter = "award"},Filter {_filtFilter = "favorite"},Filter {_filtFilter = "night"},Filter {_filtFilter = "rating"},Filter {_filtFilter = "score"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_user :: Icon
fa_user = Icon {_icoName = IconName {_icnName = "User"}, _icoId = IconId {_iidId = "user"}, _icoChar = Unicode {_uniChar = '\61447'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "head"},Filter {_filtFilter = "man"},Filter {_filtFilter = "person"},Filter {_filtFilter = "profile"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_film :: Icon
fa_film = Icon {_icoName = IconName {_icnName = "Film"}, _icoId = IconId {_iidId = "film"}, _icoChar = Unicode {_uniChar = '\61448'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "movie"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_th_large :: Icon
fa_th_large = Icon {_icoName = IconName {_icnName = "th-large"}, _icoId = IconId {_iidId = "th-large"}, _icoChar = Unicode {_uniChar = '\61449'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "blocks"},Filter {_filtFilter = "boxes"},Filter {_filtFilter = "grid"},Filter {_filtFilter = "squares"}]), _icoCategories = fromList [Category {_catCategory = "Text Editor Icons"}]}

fa_th :: Icon
fa_th = Icon {_icoName = IconName {_icnName = "th"}, _icoId = IconId {_iidId = "th"}, _icoChar = Unicode {_uniChar = '\61450'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "blocks"},Filter {_filtFilter = "boxes"},Filter {_filtFilter = "grid"},Filter {_filtFilter = "squares"}]), _icoCategories = fromList [Category {_catCategory = "Text Editor Icons"}]}

fa_th_list :: Icon
fa_th_list = Icon {_icoName = IconName {_icnName = "th-list"}, _icoId = IconId {_iidId = "th-list"}, _icoChar = Unicode {_uniChar = '\61451'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "checklist"},Filter {_filtFilter = "completed"},Filter {_filtFilter = "done"},Filter {_filtFilter = "finished"},Filter {_filtFilter = "ol"},Filter {_filtFilter = "todo"},Filter {_filtFilter = "ul"}]), _icoCategories = fromList [Category {_catCategory = "Text Editor Icons"}]}

fa_check :: Icon
fa_check = Icon {_icoName = IconName {_icnName = "Check"}, _icoId = IconId {_iidId = "check"}, _icoChar = Unicode {_uniChar = '\61452'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "accept"},Filter {_filtFilter = "agree"},Filter {_filtFilter = "checkmark"},Filter {_filtFilter = "confirm"},Filter {_filtFilter = "done"},Filter {_filtFilter = "ok"},Filter {_filtFilter = "tick"},Filter {_filtFilter = "todo"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_times :: Icon
fa_times = Icon {_icoName = IconName {_icnName = "Times"}, _icoId = IconId {_iidId = "times"}, _icoChar = Unicode {_uniChar = '\61453'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "close"},Filter {_filtFilter = "cross"},Filter {_filtFilter = "exit"},Filter {_filtFilter = "x"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_search_plus :: Icon
fa_search_plus = Icon {_icoName = IconName {_icnName = "Search Plus"}, _icoId = IconId {_iidId = "search-plus"}, _icoChar = Unicode {_uniChar = '\61454'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "bigger"},Filter {_filtFilter = "enlarge"},Filter {_filtFilter = "magnify"},Filter {_filtFilter = "zoom"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_search_minus :: Icon
fa_search_minus = Icon {_icoName = IconName {_icnName = "Search Minus"}, _icoId = IconId {_iidId = "search-minus"}, _icoChar = Unicode {_uniChar = '\61456'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "magnify"},Filter {_filtFilter = "minify"},Filter {_filtFilter = "smaller"},Filter {_filtFilter = "zoom"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_power_off :: Icon
fa_power_off = Icon {_icoName = IconName {_icnName = "Power Off"}, _icoId = IconId {_iidId = "power-off"}, _icoChar = Unicode {_uniChar = '\61457'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "on"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_signal :: Icon
fa_signal = Icon {_icoName = IconName {_icnName = "signal"}, _icoId = IconId {_iidId = "signal"}, _icoChar = Unicode {_uniChar = '\61458'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "bars"},Filter {_filtFilter = "graph"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_cog :: Icon
fa_cog = Icon {_icoName = IconName {_icnName = "cog"}, _icoId = IconId {_iidId = "cog"}, _icoChar = Unicode {_uniChar = '\61459'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "settings"}]), _icoCategories = fromList [Category {_catCategory = "Spinner Icons"},Category {_catCategory = "Web Application Icons"}]}

fa_trash_o :: Icon
fa_trash_o = Icon {_icoName = IconName {_icnName = "Trash Outlined"}, _icoId = IconId {_iidId = "trash-o"}, _icoChar = Unicode {_uniChar = '\61460'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "delete"},Filter {_filtFilter = "garbage"},Filter {_filtFilter = "hide"},Filter {_filtFilter = "remove"},Filter {_filtFilter = "trash"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_home :: Icon
fa_home = Icon {_icoName = IconName {_icnName = "home"}, _icoId = IconId {_iidId = "home"}, _icoChar = Unicode {_uniChar = '\61461'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "house"},Filter {_filtFilter = "main"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_file_o :: Icon
fa_file_o = Icon {_icoName = IconName {_icnName = "File Outlined"}, _icoId = IconId {_iidId = "file-o"}, _icoChar = Unicode {_uniChar = '\61462'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "document"},Filter {_filtFilter = "new"},Filter {_filtFilter = "page"},Filter {_filtFilter = "pdf"}]), _icoCategories = fromList [Category {_catCategory = "File Type Icons"},Category {_catCategory = "Text Editor Icons"}]}

fa_clock_o :: Icon
fa_clock_o = Icon {_icoName = IconName {_icnName = "Clock Outlined"}, _icoId = IconId {_iidId = "clock-o"}, _icoChar = Unicode {_uniChar = '\61463'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "late"},Filter {_filtFilter = "timer"},Filter {_filtFilter = "timestamp"},Filter {_filtFilter = "watch"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_road :: Icon
fa_road = Icon {_icoName = IconName {_icnName = "road"}, _icoId = IconId {_iidId = "road"}, _icoChar = Unicode {_uniChar = '\61464'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "street"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_download :: Icon
fa_download = Icon {_icoName = IconName {_icnName = "Download"}, _icoId = IconId {_iidId = "download"}, _icoChar = Unicode {_uniChar = '\61465'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "import"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_arrow_circle_o_down :: Icon
fa_arrow_circle_o_down = Icon {_icoName = IconName {_icnName = "Arrow Circle Outlined Down"}, _icoId = IconId {_iidId = "arrow-circle-o-down"}, _icoChar = Unicode {_uniChar = '\61466'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "download"}]), _icoCategories = fromList [Category {_catCategory = "Directional Icons"}]}

fa_arrow_circle_o_up :: Icon
fa_arrow_circle_o_up = Icon {_icoName = IconName {_icnName = "Arrow Circle Outlined Up"}, _icoId = IconId {_iidId = "arrow-circle-o-up"}, _icoChar = Unicode {_uniChar = '\61467'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Directional Icons"}]}

fa_inbox :: Icon
fa_inbox = Icon {_icoName = IconName {_icnName = "inbox"}, _icoId = IconId {_iidId = "inbox"}, _icoChar = Unicode {_uniChar = '\61468'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_play_circle_o :: Icon
fa_play_circle_o = Icon {_icoName = IconName {_icnName = "Play Circle Outlined"}, _icoId = IconId {_iidId = "play-circle-o"}, _icoChar = Unicode {_uniChar = '\61469'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Video Player Icons"}]}

fa_repeat :: Icon
fa_repeat = Icon {_icoName = IconName {_icnName = "Repeat"}, _icoId = IconId {_iidId = "repeat"}, _icoChar = Unicode {_uniChar = '\61470'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "forward"},Filter {_filtFilter = "redo"}]), _icoCategories = fromList [Category {_catCategory = "Text Editor Icons"}]}

fa_refresh :: Icon
fa_refresh = Icon {_icoName = IconName {_icnName = "refresh"}, _icoId = IconId {_iidId = "refresh"}, _icoChar = Unicode {_uniChar = '\61473'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "reload"},Filter {_filtFilter = "sync"}]), _icoCategories = fromList [Category {_catCategory = "Spinner Icons"},Category {_catCategory = "Web Application Icons"}]}

fa_list_alt :: Icon
fa_list_alt = Icon {_icoName = IconName {_icnName = "list-alt"}, _icoId = IconId {_iidId = "list-alt"}, _icoChar = Unicode {_uniChar = '\61474'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "checklist"},Filter {_filtFilter = "completed"},Filter {_filtFilter = "done"},Filter {_filtFilter = "finished"},Filter {_filtFilter = "ol"},Filter {_filtFilter = "todo"},Filter {_filtFilter = "ul"}]), _icoCategories = fromList [Category {_catCategory = "Text Editor Icons"}]}

fa_lock :: Icon
fa_lock = Icon {_icoName = IconName {_icnName = "lock"}, _icoId = IconId {_iidId = "lock"}, _icoChar = Unicode {_uniChar = '\61475'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "admin"},Filter {_filtFilter = "protect"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_flag :: Icon
fa_flag = Icon {_icoName = IconName {_icnName = "flag"}, _icoId = IconId {_iidId = "flag"}, _icoChar = Unicode {_uniChar = '\61476'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "notification"},Filter {_filtFilter = "notify"},Filter {_filtFilter = "report"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_headphones :: Icon
fa_headphones = Icon {_icoName = IconName {_icnName = "headphones"}, _icoId = IconId {_iidId = "headphones"}, _icoChar = Unicode {_uniChar = '\61477'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "audio"},Filter {_filtFilter = "listen"},Filter {_filtFilter = "music"},Filter {_filtFilter = "sound"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_volume_off :: Icon
fa_volume_off = Icon {_icoName = IconName {_icnName = "volume-off"}, _icoId = IconId {_iidId = "volume-off"}, _icoChar = Unicode {_uniChar = '\61478'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "audio"},Filter {_filtFilter = "music"},Filter {_filtFilter = "mute"},Filter {_filtFilter = "sound"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_volume_down :: Icon
fa_volume_down = Icon {_icoName = IconName {_icnName = "volume-down"}, _icoId = IconId {_iidId = "volume-down"}, _icoChar = Unicode {_uniChar = '\61479'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "audio"},Filter {_filtFilter = "lower"},Filter {_filtFilter = "music"},Filter {_filtFilter = "quieter"},Filter {_filtFilter = "sound"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_volume_up :: Icon
fa_volume_up = Icon {_icoName = IconName {_icnName = "volume-up"}, _icoId = IconId {_iidId = "volume-up"}, _icoChar = Unicode {_uniChar = '\61480'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "audio"},Filter {_filtFilter = "higher"},Filter {_filtFilter = "louder"},Filter {_filtFilter = "music"},Filter {_filtFilter = "sound"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_qrcode :: Icon
fa_qrcode = Icon {_icoName = IconName {_icnName = "qrcode"}, _icoId = IconId {_iidId = "qrcode"}, _icoChar = Unicode {_uniChar = '\61481'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "scan"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_barcode :: Icon
fa_barcode = Icon {_icoName = IconName {_icnName = "barcode"}, _icoId = IconId {_iidId = "barcode"}, _icoChar = Unicode {_uniChar = '\61482'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "scan"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_tag :: Icon
fa_tag = Icon {_icoName = IconName {_icnName = "tag"}, _icoId = IconId {_iidId = "tag"}, _icoChar = Unicode {_uniChar = '\61483'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "label"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_tags :: Icon
fa_tags = Icon {_icoName = IconName {_icnName = "tags"}, _icoId = IconId {_iidId = "tags"}, _icoChar = Unicode {_uniChar = '\61484'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "labels"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_book :: Icon
fa_book = Icon {_icoName = IconName {_icnName = "book"}, _icoId = IconId {_iidId = "book"}, _icoChar = Unicode {_uniChar = '\61485'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "documentation"},Filter {_filtFilter = "read"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_bookmark :: Icon
fa_bookmark = Icon {_icoName = IconName {_icnName = "bookmark"}, _icoId = IconId {_iidId = "bookmark"}, _icoChar = Unicode {_uniChar = '\61486'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "save"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_print :: Icon
fa_print = Icon {_icoName = IconName {_icnName = "print"}, _icoId = IconId {_iidId = "print"}, _icoChar = Unicode {_uniChar = '\61487'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_camera :: Icon
fa_camera = Icon {_icoName = IconName {_icnName = "camera"}, _icoId = IconId {_iidId = "camera"}, _icoChar = Unicode {_uniChar = '\61488'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "photo"},Filter {_filtFilter = "picture"},Filter {_filtFilter = "record"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_font :: Icon
fa_font = Icon {_icoName = IconName {_icnName = "font"}, _icoId = IconId {_iidId = "font"}, _icoChar = Unicode {_uniChar = '\61489'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "text"}]), _icoCategories = fromList [Category {_catCategory = "Text Editor Icons"}]}

fa_bold :: Icon
fa_bold = Icon {_icoName = IconName {_icnName = "bold"}, _icoId = IconId {_iidId = "bold"}, _icoChar = Unicode {_uniChar = '\61490'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Text Editor Icons"}]}

fa_italic :: Icon
fa_italic = Icon {_icoName = IconName {_icnName = "italic"}, _icoId = IconId {_iidId = "italic"}, _icoChar = Unicode {_uniChar = '\61491'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "italics"}]), _icoCategories = fromList [Category {_catCategory = "Text Editor Icons"}]}

fa_text_height :: Icon
fa_text_height = Icon {_icoName = IconName {_icnName = "text-height"}, _icoId = IconId {_iidId = "text-height"}, _icoChar = Unicode {_uniChar = '\61492'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Text Editor Icons"}]}

fa_text_width :: Icon
fa_text_width = Icon {_icoName = IconName {_icnName = "text-width"}, _icoId = IconId {_iidId = "text-width"}, _icoChar = Unicode {_uniChar = '\61493'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Text Editor Icons"}]}

fa_align_left :: Icon
fa_align_left = Icon {_icoName = IconName {_icnName = "align-left"}, _icoId = IconId {_iidId = "align-left"}, _icoChar = Unicode {_uniChar = '\61494'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "text"}]), _icoCategories = fromList [Category {_catCategory = "Text Editor Icons"}]}

fa_align_center :: Icon
fa_align_center = Icon {_icoName = IconName {_icnName = "align-center"}, _icoId = IconId {_iidId = "align-center"}, _icoChar = Unicode {_uniChar = '\61495'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "middle"},Filter {_filtFilter = "text"}]), _icoCategories = fromList [Category {_catCategory = "Text Editor Icons"}]}

fa_align_right :: Icon
fa_align_right = Icon {_icoName = IconName {_icnName = "align-right"}, _icoId = IconId {_iidId = "align-right"}, _icoChar = Unicode {_uniChar = '\61496'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "text"}]), _icoCategories = fromList [Category {_catCategory = "Text Editor Icons"}]}

fa_align_justify :: Icon
fa_align_justify = Icon {_icoName = IconName {_icnName = "align-justify"}, _icoId = IconId {_iidId = "align-justify"}, _icoChar = Unicode {_uniChar = '\61497'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "text"}]), _icoCategories = fromList [Category {_catCategory = "Text Editor Icons"}]}

fa_list :: Icon
fa_list = Icon {_icoName = IconName {_icnName = "list"}, _icoId = IconId {_iidId = "list"}, _icoChar = Unicode {_uniChar = '\61498'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "checklist"},Filter {_filtFilter = "completed"},Filter {_filtFilter = "done"},Filter {_filtFilter = "finished"},Filter {_filtFilter = "ol"},Filter {_filtFilter = "todo"},Filter {_filtFilter = "ul"}]), _icoCategories = fromList [Category {_catCategory = "Text Editor Icons"}]}

fa_outdent :: Icon
fa_outdent = Icon {_icoName = IconName {_icnName = "Outdent"}, _icoId = IconId {_iidId = "outdent"}, _icoChar = Unicode {_uniChar = '\61499'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Text Editor Icons"}]}

fa_indent :: Icon
fa_indent = Icon {_icoName = IconName {_icnName = "Indent"}, _icoId = IconId {_iidId = "indent"}, _icoChar = Unicode {_uniChar = '\61500'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Text Editor Icons"}]}

fa_video_camera :: Icon
fa_video_camera = Icon {_icoName = IconName {_icnName = "Video Camera"}, _icoId = IconId {_iidId = "video-camera"}, _icoChar = Unicode {_uniChar = '\61501'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "film"},Filter {_filtFilter = "movie"},Filter {_filtFilter = "record"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_picture_o :: Icon
fa_picture_o = Icon {_icoName = IconName {_icnName = "Picture Outlined"}, _icoId = IconId {_iidId = "picture-o"}, _icoChar = Unicode {_uniChar = '\61502'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_pencil :: Icon
fa_pencil = Icon {_icoName = IconName {_icnName = "pencil"}, _icoId = IconId {_iidId = "pencil"}, _icoChar = Unicode {_uniChar = '\61504'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "edit"},Filter {_filtFilter = "update"},Filter {_filtFilter = "write"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_map_marker :: Icon
fa_map_marker = Icon {_icoName = IconName {_icnName = "map-marker"}, _icoId = IconId {_iidId = "map-marker"}, _icoChar = Unicode {_uniChar = '\61505'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "address"},Filter {_filtFilter = "coordinates"},Filter {_filtFilter = "localize"},Filter {_filtFilter = "location"},Filter {_filtFilter = "map"},Filter {_filtFilter = "pin"},Filter {_filtFilter = "place"},Filter {_filtFilter = "travel"},Filter {_filtFilter = "where"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_adjust :: Icon
fa_adjust = Icon {_icoName = IconName {_icnName = "adjust"}, _icoId = IconId {_iidId = "adjust"}, _icoChar = Unicode {_uniChar = '\61506'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "contrast"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_tint :: Icon
fa_tint = Icon {_icoName = IconName {_icnName = "tint"}, _icoId = IconId {_iidId = "tint"}, _icoChar = Unicode {_uniChar = '\61507'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "drop"},Filter {_filtFilter = "droplet"},Filter {_filtFilter = "raindrop"},Filter {_filtFilter = "waterdrop"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_pencil_square_o :: Icon
fa_pencil_square_o = Icon {_icoName = IconName {_icnName = "Pencil Square Outlined"}, _icoId = IconId {_iidId = "pencil-square-o"}, _icoChar = Unicode {_uniChar = '\61508'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "edit"},Filter {_filtFilter = "update"},Filter {_filtFilter = "write"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_share_square_o :: Icon
fa_share_square_o = Icon {_icoName = IconName {_icnName = "Share Square Outlined"}, _icoId = IconId {_iidId = "share-square-o"}, _icoChar = Unicode {_uniChar = '\61509'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "arrow"},Filter {_filtFilter = "send"},Filter {_filtFilter = "social"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_check_square_o :: Icon
fa_check_square_o = Icon {_icoName = IconName {_icnName = "Check Square Outlined"}, _icoId = IconId {_iidId = "check-square-o"}, _icoChar = Unicode {_uniChar = '\61510'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "accept"},Filter {_filtFilter = "agree"},Filter {_filtFilter = "confirm"},Filter {_filtFilter = "done"},Filter {_filtFilter = "ok"},Filter {_filtFilter = "todo"}]), _icoCategories = fromList [Category {_catCategory = "Form Control Icons"},Category {_catCategory = "Web Application Icons"}]}

fa_arrows :: Icon
fa_arrows = Icon {_icoName = IconName {_icnName = "Arrows"}, _icoId = IconId {_iidId = "arrows"}, _icoChar = Unicode {_uniChar = '\61511'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "move"},Filter {_filtFilter = "reorder"},Filter {_filtFilter = "resize"}]), _icoCategories = fromList [Category {_catCategory = "Directional Icons"},Category {_catCategory = "Web Application Icons"}]}

fa_step_backward :: Icon
fa_step_backward = Icon {_icoName = IconName {_icnName = "step-backward"}, _icoId = IconId {_iidId = "step-backward"}, _icoChar = Unicode {_uniChar = '\61512'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "beginning"},Filter {_filtFilter = "first"},Filter {_filtFilter = "previous"},Filter {_filtFilter = "rewind"},Filter {_filtFilter = "start"}]), _icoCategories = fromList [Category {_catCategory = "Video Player Icons"}]}

fa_fast_backward :: Icon
fa_fast_backward = Icon {_icoName = IconName {_icnName = "fast-backward"}, _icoId = IconId {_iidId = "fast-backward"}, _icoChar = Unicode {_uniChar = '\61513'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "beginning"},Filter {_filtFilter = "first"},Filter {_filtFilter = "previous"},Filter {_filtFilter = "rewind"},Filter {_filtFilter = "start"}]), _icoCategories = fromList [Category {_catCategory = "Video Player Icons"}]}

fa_backward :: Icon
fa_backward = Icon {_icoName = IconName {_icnName = "backward"}, _icoId = IconId {_iidId = "backward"}, _icoChar = Unicode {_uniChar = '\61514'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "previous"},Filter {_filtFilter = "rewind"}]), _icoCategories = fromList [Category {_catCategory = "Video Player Icons"}]}

fa_play :: Icon
fa_play = Icon {_icoName = IconName {_icnName = "play"}, _icoId = IconId {_iidId = "play"}, _icoChar = Unicode {_uniChar = '\61515'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "music"},Filter {_filtFilter = "playing"},Filter {_filtFilter = "sound"},Filter {_filtFilter = "start"}]), _icoCategories = fromList [Category {_catCategory = "Video Player Icons"}]}

fa_pause :: Icon
fa_pause = Icon {_icoName = IconName {_icnName = "pause"}, _icoId = IconId {_iidId = "pause"}, _icoChar = Unicode {_uniChar = '\61516'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "wait"}]), _icoCategories = fromList [Category {_catCategory = "Video Player Icons"}]}

fa_stop :: Icon
fa_stop = Icon {_icoName = IconName {_icnName = "stop"}, _icoId = IconId {_iidId = "stop"}, _icoChar = Unicode {_uniChar = '\61517'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "block"},Filter {_filtFilter = "box"},Filter {_filtFilter = "square"}]), _icoCategories = fromList [Category {_catCategory = "Video Player Icons"}]}

fa_forward :: Icon
fa_forward = Icon {_icoName = IconName {_icnName = "forward"}, _icoId = IconId {_iidId = "forward"}, _icoChar = Unicode {_uniChar = '\61518'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "forward"},Filter {_filtFilter = "next"}]), _icoCategories = fromList [Category {_catCategory = "Video Player Icons"}]}

fa_fast_forward :: Icon
fa_fast_forward = Icon {_icoName = IconName {_icnName = "fast-forward"}, _icoId = IconId {_iidId = "fast-forward"}, _icoChar = Unicode {_uniChar = '\61520'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "end"},Filter {_filtFilter = "last"},Filter {_filtFilter = "next"}]), _icoCategories = fromList [Category {_catCategory = "Video Player Icons"}]}

fa_step_forward :: Icon
fa_step_forward = Icon {_icoName = IconName {_icnName = "step-forward"}, _icoId = IconId {_iidId = "step-forward"}, _icoChar = Unicode {_uniChar = '\61521'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "end"},Filter {_filtFilter = "last"},Filter {_filtFilter = "next"}]), _icoCategories = fromList [Category {_catCategory = "Video Player Icons"}]}

fa_eject :: Icon
fa_eject = Icon {_icoName = IconName {_icnName = "eject"}, _icoId = IconId {_iidId = "eject"}, _icoChar = Unicode {_uniChar = '\61522'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Video Player Icons"}]}

fa_chevron_left :: Icon
fa_chevron_left = Icon {_icoName = IconName {_icnName = "chevron-left"}, _icoId = IconId {_iidId = "chevron-left"}, _icoChar = Unicode {_uniChar = '\61523'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "back"},Filter {_filtFilter = "bracket"},Filter {_filtFilter = "previous"}]), _icoCategories = fromList [Category {_catCategory = "Directional Icons"}]}

fa_chevron_right :: Icon
fa_chevron_right = Icon {_icoName = IconName {_icnName = "chevron-right"}, _icoId = IconId {_iidId = "chevron-right"}, _icoChar = Unicode {_uniChar = '\61524'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "bracket"},Filter {_filtFilter = "forward"},Filter {_filtFilter = "next"}]), _icoCategories = fromList [Category {_catCategory = "Directional Icons"}]}

fa_plus_circle :: Icon
fa_plus_circle = Icon {_icoName = IconName {_icnName = "Plus Circle"}, _icoId = IconId {_iidId = "plus-circle"}, _icoChar = Unicode {_uniChar = '\61525'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "add"},Filter {_filtFilter = "create"},Filter {_filtFilter = "expand"},Filter {_filtFilter = "new"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_minus_circle :: Icon
fa_minus_circle = Icon {_icoName = IconName {_icnName = "Minus Circle"}, _icoId = IconId {_iidId = "minus-circle"}, _icoChar = Unicode {_uniChar = '\61526'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "delete"},Filter {_filtFilter = "hide"},Filter {_filtFilter = "remove"},Filter {_filtFilter = "trash"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_times_circle :: Icon
fa_times_circle = Icon {_icoName = IconName {_icnName = "Times Circle"}, _icoId = IconId {_iidId = "times-circle"}, _icoChar = Unicode {_uniChar = '\61527'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "close"},Filter {_filtFilter = "exit"},Filter {_filtFilter = "x"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_check_circle :: Icon
fa_check_circle = Icon {_icoName = IconName {_icnName = "Check Circle"}, _icoId = IconId {_iidId = "check-circle"}, _icoChar = Unicode {_uniChar = '\61528'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "accept"},Filter {_filtFilter = "agree"},Filter {_filtFilter = "confirm"},Filter {_filtFilter = "done"},Filter {_filtFilter = "ok"},Filter {_filtFilter = "todo"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_question_circle :: Icon
fa_question_circle = Icon {_icoName = IconName {_icnName = "Question Circle"}, _icoId = IconId {_iidId = "question-circle"}, _icoChar = Unicode {_uniChar = '\61529'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "help"},Filter {_filtFilter = "information"},Filter {_filtFilter = "support"},Filter {_filtFilter = "unknown"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_info_circle :: Icon
fa_info_circle = Icon {_icoName = IconName {_icnName = "Info Circle"}, _icoId = IconId {_iidId = "info-circle"}, _icoChar = Unicode {_uniChar = '\61530'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "details"},Filter {_filtFilter = "help"},Filter {_filtFilter = "information"},Filter {_filtFilter = "more"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_crosshairs :: Icon
fa_crosshairs = Icon {_icoName = IconName {_icnName = "Crosshairs"}, _icoId = IconId {_iidId = "crosshairs"}, _icoChar = Unicode {_uniChar = '\61531'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "picker"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_times_circle_o :: Icon
fa_times_circle_o = Icon {_icoName = IconName {_icnName = "Times Circle Outlined"}, _icoId = IconId {_iidId = "times-circle-o"}, _icoChar = Unicode {_uniChar = '\61532'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "close"},Filter {_filtFilter = "exit"},Filter {_filtFilter = "x"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_check_circle_o :: Icon
fa_check_circle_o = Icon {_icoName = IconName {_icnName = "Check Circle Outlined"}, _icoId = IconId {_iidId = "check-circle-o"}, _icoChar = Unicode {_uniChar = '\61533'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "accept"},Filter {_filtFilter = "agree"},Filter {_filtFilter = "confirm"},Filter {_filtFilter = "done"},Filter {_filtFilter = "ok"},Filter {_filtFilter = "todo"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_ban :: Icon
fa_ban = Icon {_icoName = IconName {_icnName = "ban"}, _icoId = IconId {_iidId = "ban"}, _icoChar = Unicode {_uniChar = '\61534'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "abort"},Filter {_filtFilter = "block"},Filter {_filtFilter = "cancel"},Filter {_filtFilter = "delete"},Filter {_filtFilter = "hide"},Filter {_filtFilter = "remove"},Filter {_filtFilter = "stop"},Filter {_filtFilter = "trash"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_arrow_left :: Icon
fa_arrow_left = Icon {_icoName = IconName {_icnName = "arrow-left"}, _icoId = IconId {_iidId = "arrow-left"}, _icoChar = Unicode {_uniChar = '\61536'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "back"},Filter {_filtFilter = "previous"}]), _icoCategories = fromList [Category {_catCategory = "Directional Icons"}]}

fa_arrow_right :: Icon
fa_arrow_right = Icon {_icoName = IconName {_icnName = "arrow-right"}, _icoId = IconId {_iidId = "arrow-right"}, _icoChar = Unicode {_uniChar = '\61537'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "forward"},Filter {_filtFilter = "next"}]), _icoCategories = fromList [Category {_catCategory = "Directional Icons"}]}

fa_arrow_up :: Icon
fa_arrow_up = Icon {_icoName = IconName {_icnName = "arrow-up"}, _icoId = IconId {_iidId = "arrow-up"}, _icoChar = Unicode {_uniChar = '\61538'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Directional Icons"}]}

fa_arrow_down :: Icon
fa_arrow_down = Icon {_icoName = IconName {_icnName = "arrow-down"}, _icoId = IconId {_iidId = "arrow-down"}, _icoChar = Unicode {_uniChar = '\61539'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "download"}]), _icoCategories = fromList [Category {_catCategory = "Directional Icons"}]}

fa_share :: Icon
fa_share = Icon {_icoName = IconName {_icnName = "Share"}, _icoId = IconId {_iidId = "share"}, _icoChar = Unicode {_uniChar = '\61540'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_expand :: Icon
fa_expand = Icon {_icoName = IconName {_icnName = "Expand"}, _icoId = IconId {_iidId = "expand"}, _icoChar = Unicode {_uniChar = '\61541'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "bigger"},Filter {_filtFilter = "enlarge"},Filter {_filtFilter = "resize"}]), _icoCategories = fromList [Category {_catCategory = "Video Player Icons"}]}

fa_compress :: Icon
fa_compress = Icon {_icoName = IconName {_icnName = "Compress"}, _icoId = IconId {_iidId = "compress"}, _icoChar = Unicode {_uniChar = '\61542'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "collapse"},Filter {_filtFilter = "combine"},Filter {_filtFilter = "contract"},Filter {_filtFilter = "merge"},Filter {_filtFilter = "smaller"}]), _icoCategories = fromList [Category {_catCategory = "Video Player Icons"}]}

fa_plus :: Icon
fa_plus = Icon {_icoName = IconName {_icnName = "plus"}, _icoId = IconId {_iidId = "plus"}, _icoChar = Unicode {_uniChar = '\61543'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "add"},Filter {_filtFilter = "create"},Filter {_filtFilter = "expand"},Filter {_filtFilter = "new"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_minus :: Icon
fa_minus = Icon {_icoName = IconName {_icnName = "minus"}, _icoId = IconId {_iidId = "minus"}, _icoChar = Unicode {_uniChar = '\61544'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "collapse"},Filter {_filtFilter = "delete"},Filter {_filtFilter = "hide"},Filter {_filtFilter = "minify"},Filter {_filtFilter = "remove"},Filter {_filtFilter = "trash"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_asterisk :: Icon
fa_asterisk = Icon {_icoName = IconName {_icnName = "asterisk"}, _icoId = IconId {_iidId = "asterisk"}, _icoChar = Unicode {_uniChar = '\61545'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "details"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_exclamation_circle :: Icon
fa_exclamation_circle = Icon {_icoName = IconName {_icnName = "Exclamation Circle"}, _icoId = IconId {_iidId = "exclamation-circle"}, _icoChar = Unicode {_uniChar = '\61546'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "alert"},Filter {_filtFilter = "error"},Filter {_filtFilter = "notification"},Filter {_filtFilter = "problem"},Filter {_filtFilter = "warning"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_gift :: Icon
fa_gift = Icon {_icoName = IconName {_icnName = "gift"}, _icoId = IconId {_iidId = "gift"}, _icoChar = Unicode {_uniChar = '\61547'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "present"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_leaf :: Icon
fa_leaf = Icon {_icoName = IconName {_icnName = "leaf"}, _icoId = IconId {_iidId = "leaf"}, _icoChar = Unicode {_uniChar = '\61548'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "eco"},Filter {_filtFilter = "nature"},Filter {_filtFilter = "plant"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_fire :: Icon
fa_fire = Icon {_icoName = IconName {_icnName = "fire"}, _icoId = IconId {_iidId = "fire"}, _icoChar = Unicode {_uniChar = '\61549'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "flame"},Filter {_filtFilter = "hot"},Filter {_filtFilter = "popular"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_eye :: Icon
fa_eye = Icon {_icoName = IconName {_icnName = "Eye"}, _icoId = IconId {_iidId = "eye"}, _icoChar = Unicode {_uniChar = '\61550'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "show"},Filter {_filtFilter = "views"},Filter {_filtFilter = "visible"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_eye_slash :: Icon
fa_eye_slash = Icon {_icoName = IconName {_icnName = "Eye Slash"}, _icoId = IconId {_iidId = "eye-slash"}, _icoChar = Unicode {_uniChar = '\61552'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "hide"},Filter {_filtFilter = "show"},Filter {_filtFilter = "toggle"},Filter {_filtFilter = "views"},Filter {_filtFilter = "visible"},Filter {_filtFilter = "visiblity"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_exclamation_triangle :: Icon
fa_exclamation_triangle = Icon {_icoName = IconName {_icnName = "Exclamation Triangle"}, _icoId = IconId {_iidId = "exclamation-triangle"}, _icoChar = Unicode {_uniChar = '\61553'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "alert"},Filter {_filtFilter = "error"},Filter {_filtFilter = "notification"},Filter {_filtFilter = "problem"},Filter {_filtFilter = "warning"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_plane :: Icon
fa_plane = Icon {_icoName = IconName {_icnName = "plane"}, _icoId = IconId {_iidId = "plane"}, _icoChar = Unicode {_uniChar = '\61554'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "airplane"},Filter {_filtFilter = "destination"},Filter {_filtFilter = "fly"},Filter {_filtFilter = "location"},Filter {_filtFilter = "mode"},Filter {_filtFilter = "travel"},Filter {_filtFilter = "trip"}]), _icoCategories = fromList [Category {_catCategory = "Transportation Icons"},Category {_catCategory = "Web Application Icons"}]}

fa_calendar :: Icon
fa_calendar = Icon {_icoName = IconName {_icnName = "calendar"}, _icoId = IconId {_iidId = "calendar"}, _icoChar = Unicode {_uniChar = '\61555'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "date"},Filter {_filtFilter = "event"},Filter {_filtFilter = "time"},Filter {_filtFilter = "when"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_random :: Icon
fa_random = Icon {_icoName = IconName {_icnName = "random"}, _icoId = IconId {_iidId = "random"}, _icoChar = Unicode {_uniChar = '\61556'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "shuffle"},Filter {_filtFilter = "sort"}]), _icoCategories = fromList [Category {_catCategory = "Video Player Icons"},Category {_catCategory = "Web Application Icons"}]}

fa_comment :: Icon
fa_comment = Icon {_icoName = IconName {_icnName = "comment"}, _icoId = IconId {_iidId = "comment"}, _icoChar = Unicode {_uniChar = '\61557'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "bubble"},Filter {_filtFilter = "chat"},Filter {_filtFilter = "feedback"},Filter {_filtFilter = "message"},Filter {_filtFilter = "note"},Filter {_filtFilter = "notification"},Filter {_filtFilter = "sms"},Filter {_filtFilter = "speech"},Filter {_filtFilter = "texting"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_magnet :: Icon
fa_magnet = Icon {_icoName = IconName {_icnName = "magnet"}, _icoId = IconId {_iidId = "magnet"}, _icoChar = Unicode {_uniChar = '\61558'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_chevron_up :: Icon
fa_chevron_up = Icon {_icoName = IconName {_icnName = "chevron-up"}, _icoId = IconId {_iidId = "chevron-up"}, _icoChar = Unicode {_uniChar = '\61559'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Directional Icons"}]}

fa_chevron_down :: Icon
fa_chevron_down = Icon {_icoName = IconName {_icnName = "chevron-down"}, _icoId = IconId {_iidId = "chevron-down"}, _icoChar = Unicode {_uniChar = '\61560'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Directional Icons"}]}

fa_retweet :: Icon
fa_retweet = Icon {_icoName = IconName {_icnName = "retweet"}, _icoId = IconId {_iidId = "retweet"}, _icoChar = Unicode {_uniChar = '\61561'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "refresh"},Filter {_filtFilter = "reload"},Filter {_filtFilter = "share"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_shopping_cart :: Icon
fa_shopping_cart = Icon {_icoName = IconName {_icnName = "shopping-cart"}, _icoId = IconId {_iidId = "shopping-cart"}, _icoChar = Unicode {_uniChar = '\61562'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "buy"},Filter {_filtFilter = "checkout"},Filter {_filtFilter = "payment"},Filter {_filtFilter = "purchase"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_folder :: Icon
fa_folder = Icon {_icoName = IconName {_icnName = "Folder"}, _icoId = IconId {_iidId = "folder"}, _icoChar = Unicode {_uniChar = '\61563'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_folder_open :: Icon
fa_folder_open = Icon {_icoName = IconName {_icnName = "Folder Open"}, _icoId = IconId {_iidId = "folder-open"}, _icoChar = Unicode {_uniChar = '\61564'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_arrows_v :: Icon
fa_arrows_v = Icon {_icoName = IconName {_icnName = "Arrows Vertical"}, _icoId = IconId {_iidId = "arrows-v"}, _icoChar = Unicode {_uniChar = '\61565'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "resize"}]), _icoCategories = fromList [Category {_catCategory = "Directional Icons"},Category {_catCategory = "Web Application Icons"}]}

fa_arrows_h :: Icon
fa_arrows_h = Icon {_icoName = IconName {_icnName = "Arrows Horizontal"}, _icoId = IconId {_iidId = "arrows-h"}, _icoChar = Unicode {_uniChar = '\61566'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "resize"}]), _icoCategories = fromList [Category {_catCategory = "Directional Icons"},Category {_catCategory = "Web Application Icons"}]}

fa_bar_chart :: Icon
fa_bar_chart = Icon {_icoName = IconName {_icnName = "Bar Chart"}, _icoId = IconId {_iidId = "bar-chart"}, _icoChar = Unicode {_uniChar = '\61568'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "analytics"},Filter {_filtFilter = "graph"}]), _icoCategories = fromList [Category {_catCategory = "Chart Icons"},Category {_catCategory = "Web Application Icons"}]}

fa_twitter_square :: Icon
fa_twitter_square = Icon {_icoName = IconName {_icnName = "Twitter Square"}, _icoId = IconId {_iidId = "twitter-square"}, _icoChar = Unicode {_uniChar = '\61569'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "social network"},Filter {_filtFilter = "tweet"}]), _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_facebook_square :: Icon
fa_facebook_square = Icon {_icoName = IconName {_icnName = "Facebook Square"}, _icoId = IconId {_iidId = "facebook-square"}, _icoChar = Unicode {_uniChar = '\61570'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "social network"}]), _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_camera_retro :: Icon
fa_camera_retro = Icon {_icoName = IconName {_icnName = "camera-retro"}, _icoId = IconId {_iidId = "camera-retro"}, _icoChar = Unicode {_uniChar = '\61571'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "photo"},Filter {_filtFilter = "picture"},Filter {_filtFilter = "record"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_key :: Icon
fa_key = Icon {_icoName = IconName {_icnName = "key"}, _icoId = IconId {_iidId = "key"}, _icoChar = Unicode {_uniChar = '\61572'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "password"},Filter {_filtFilter = "unlock"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_cogs :: Icon
fa_cogs = Icon {_icoName = IconName {_icnName = "cogs"}, _icoId = IconId {_iidId = "cogs"}, _icoChar = Unicode {_uniChar = '\61573'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "settings"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_comments :: Icon
fa_comments = Icon {_icoName = IconName {_icnName = "comments"}, _icoId = IconId {_iidId = "comments"}, _icoChar = Unicode {_uniChar = '\61574'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "chat"},Filter {_filtFilter = "conversation"},Filter {_filtFilter = "message"},Filter {_filtFilter = "notes"},Filter {_filtFilter = "notification"},Filter {_filtFilter = "sms"},Filter {_filtFilter = "texting"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_thumbs_o_up :: Icon
fa_thumbs_o_up = Icon {_icoName = IconName {_icnName = "Thumbs Up Outlined"}, _icoId = IconId {_iidId = "thumbs-o-up"}, _icoChar = Unicode {_uniChar = '\61575'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "agree"},Filter {_filtFilter = "approve"},Filter {_filtFilter = "favorite"},Filter {_filtFilter = "hand"},Filter {_filtFilter = "like"}]), _icoCategories = fromList [Category {_catCategory = "Hand Icons"},Category {_catCategory = "Web Application Icons"}]}

fa_thumbs_o_down :: Icon
fa_thumbs_o_down = Icon {_icoName = IconName {_icnName = "Thumbs Down Outlined"}, _icoId = IconId {_iidId = "thumbs-o-down"}, _icoChar = Unicode {_uniChar = '\61576'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "disagree"},Filter {_filtFilter = "disapprove"},Filter {_filtFilter = "dislike"},Filter {_filtFilter = "hand"}]), _icoCategories = fromList [Category {_catCategory = "Hand Icons"},Category {_catCategory = "Web Application Icons"}]}

fa_star_half :: Icon
fa_star_half = Icon {_icoName = IconName {_icnName = "star-half"}, _icoId = IconId {_iidId = "star-half"}, _icoChar = Unicode {_uniChar = '\61577'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "achievement"},Filter {_filtFilter = "award"},Filter {_filtFilter = "rating"},Filter {_filtFilter = "score"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_heart_o :: Icon
fa_heart_o = Icon {_icoName = IconName {_icnName = "Heart Outlined"}, _icoId = IconId {_iidId = "heart-o"}, _icoChar = Unicode {_uniChar = '\61578'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "favorite"},Filter {_filtFilter = "like"},Filter {_filtFilter = "love"}]), _icoCategories = fromList [Category {_catCategory = "Medical Icons"},Category {_catCategory = "Web Application Icons"}]}

fa_sign_out :: Icon
fa_sign_out = Icon {_icoName = IconName {_icnName = "Sign Out"}, _icoId = IconId {_iidId = "sign-out"}, _icoChar = Unicode {_uniChar = '\61579'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "arrow"},Filter {_filtFilter = "exit"},Filter {_filtFilter = "leave"},Filter {_filtFilter = "log out"},Filter {_filtFilter = "logout"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_linkedin_square :: Icon
fa_linkedin_square = Icon {_icoName = IconName {_icnName = "LinkedIn Square"}, _icoId = IconId {_iidId = "linkedin-square"}, _icoChar = Unicode {_uniChar = '\61580'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_thumb_tack :: Icon
fa_thumb_tack = Icon {_icoName = IconName {_icnName = "Thumb Tack"}, _icoId = IconId {_iidId = "thumb-tack"}, _icoChar = Unicode {_uniChar = '\61581'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "coordinates"},Filter {_filtFilter = "location"},Filter {_filtFilter = "marker"},Filter {_filtFilter = "pin"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_external_link :: Icon
fa_external_link = Icon {_icoName = IconName {_icnName = "External Link"}, _icoId = IconId {_iidId = "external-link"}, _icoChar = Unicode {_uniChar = '\61582'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "new"},Filter {_filtFilter = "open"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_sign_in :: Icon
fa_sign_in = Icon {_icoName = IconName {_icnName = "Sign In"}, _icoId = IconId {_iidId = "sign-in"}, _icoChar = Unicode {_uniChar = '\61584'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "arrow"},Filter {_filtFilter = "enter"},Filter {_filtFilter = "join"},Filter {_filtFilter = "log in"},Filter {_filtFilter = "login"},Filter {_filtFilter = "sign in"},Filter {_filtFilter = "sign up"},Filter {_filtFilter = "signin"},Filter {_filtFilter = "signup"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_trophy :: Icon
fa_trophy = Icon {_icoName = IconName {_icnName = "trophy"}, _icoId = IconId {_iidId = "trophy"}, _icoChar = Unicode {_uniChar = '\61585'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "achievement"},Filter {_filtFilter = "award"},Filter {_filtFilter = "game"},Filter {_filtFilter = "winner"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_github_square :: Icon
fa_github_square = Icon {_icoName = IconName {_icnName = "GitHub Square"}, _icoId = IconId {_iidId = "github-square"}, _icoChar = Unicode {_uniChar = '\61586'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "octocat"}]), _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_upload :: Icon
fa_upload = Icon {_icoName = IconName {_icnName = "Upload"}, _icoId = IconId {_iidId = "upload"}, _icoChar = Unicode {_uniChar = '\61587'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "import"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_lemon_o :: Icon
fa_lemon_o = Icon {_icoName = IconName {_icnName = "Lemon Outlined"}, _icoId = IconId {_iidId = "lemon-o"}, _icoChar = Unicode {_uniChar = '\61588'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [1,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "food"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_phone :: Icon
fa_phone = Icon {_icoName = IconName {_icnName = "Phone"}, _icoId = IconId {_iidId = "phone"}, _icoChar = Unicode {_uniChar = '\61589'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [2,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "call"},Filter {_filtFilter = "earphone"},Filter {_filtFilter = "number"},Filter {_filtFilter = "support"},Filter {_filtFilter = "voice"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_square_o :: Icon
fa_square_o = Icon {_icoName = IconName {_icnName = "Square Outlined"}, _icoId = IconId {_iidId = "square-o"}, _icoChar = Unicode {_uniChar = '\61590'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [2,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "block"},Filter {_filtFilter = "box"},Filter {_filtFilter = "square"}]), _icoCategories = fromList [Category {_catCategory = "Form Control Icons"},Category {_catCategory = "Web Application Icons"}]}

fa_bookmark_o :: Icon
fa_bookmark_o = Icon {_icoName = IconName {_icnName = "Bookmark Outlined"}, _icoId = IconId {_iidId = "bookmark-o"}, _icoChar = Unicode {_uniChar = '\61591'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [2,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "save"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_phone_square :: Icon
fa_phone_square = Icon {_icoName = IconName {_icnName = "Phone Square"}, _icoId = IconId {_iidId = "phone-square"}, _icoChar = Unicode {_uniChar = '\61592'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [2,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "call"},Filter {_filtFilter = "number"},Filter {_filtFilter = "support"},Filter {_filtFilter = "voice"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_twitter :: Icon
fa_twitter = Icon {_icoName = IconName {_icnName = "Twitter"}, _icoId = IconId {_iidId = "twitter"}, _icoChar = Unicode {_uniChar = '\61593'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [2,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "social network"},Filter {_filtFilter = "tweet"}]), _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_facebook :: Icon
fa_facebook = Icon {_icoName = IconName {_icnName = "Facebook"}, _icoId = IconId {_iidId = "facebook"}, _icoChar = Unicode {_uniChar = '\61594'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [2,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "social network"}]), _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_github :: Icon
fa_github = Icon {_icoName = IconName {_icnName = "GitHub"}, _icoId = IconId {_iidId = "github"}, _icoChar = Unicode {_uniChar = '\61595'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [2,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "octocat"}]), _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_unlock :: Icon
fa_unlock = Icon {_icoName = IconName {_icnName = "unlock"}, _icoId = IconId {_iidId = "unlock"}, _icoChar = Unicode {_uniChar = '\61596'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [2,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "admin"},Filter {_filtFilter = "lock"},Filter {_filtFilter = "password"},Filter {_filtFilter = "protect"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_credit_card :: Icon
fa_credit_card = Icon {_icoName = IconName {_icnName = "credit-card"}, _icoId = IconId {_iidId = "credit-card"}, _icoChar = Unicode {_uniChar = '\61597'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [2,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "buy"},Filter {_filtFilter = "checkout"},Filter {_filtFilter = "debit"},Filter {_filtFilter = "money"},Filter {_filtFilter = "payment"},Filter {_filtFilter = "purchase"}]), _icoCategories = fromList [Category {_catCategory = "Payment Icons"},Category {_catCategory = "Web Application Icons"}]}

fa_rss :: Icon
fa_rss = Icon {_icoName = IconName {_icnName = "rss"}, _icoId = IconId {_iidId = "rss"}, _icoChar = Unicode {_uniChar = '\61598'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [2,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "blog"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_hdd_o :: Icon
fa_hdd_o = Icon {_icoName = IconName {_icnName = "HDD"}, _icoId = IconId {_iidId = "hdd-o"}, _icoChar = Unicode {_uniChar = '\61600'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [2,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "hard drive"},Filter {_filtFilter = "harddrive"},Filter {_filtFilter = "save"},Filter {_filtFilter = "storage"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_bullhorn :: Icon
fa_bullhorn = Icon {_icoName = IconName {_icnName = "bullhorn"}, _icoId = IconId {_iidId = "bullhorn"}, _icoChar = Unicode {_uniChar = '\61601'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [2,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "announcement"},Filter {_filtFilter = "broadcast"},Filter {_filtFilter = "louder"},Filter {_filtFilter = "share"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_bell :: Icon
fa_bell = Icon {_icoName = IconName {_icnName = "bell"}, _icoId = IconId {_iidId = "bell"}, _icoChar = Unicode {_uniChar = '\61683'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [2,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "alert"},Filter {_filtFilter = "notification"},Filter {_filtFilter = "reminder"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_certificate :: Icon
fa_certificate = Icon {_icoName = IconName {_icnName = "certificate"}, _icoId = IconId {_iidId = "certificate"}, _icoChar = Unicode {_uniChar = '\61603'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [2,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "badge"},Filter {_filtFilter = "star"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_hand_o_right :: Icon
fa_hand_o_right = Icon {_icoName = IconName {_icnName = "Hand Outlined Right"}, _icoId = IconId {_iidId = "hand-o-right"}, _icoChar = Unicode {_uniChar = '\61604'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [2,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "finger"},Filter {_filtFilter = "forward"},Filter {_filtFilter = "next"},Filter {_filtFilter = "point"},Filter {_filtFilter = "right"}]), _icoCategories = fromList [Category {_catCategory = "Directional Icons"},Category {_catCategory = "Hand Icons"}]}

fa_hand_o_left :: Icon
fa_hand_o_left = Icon {_icoName = IconName {_icnName = "Hand Outlined Left"}, _icoId = IconId {_iidId = "hand-o-left"}, _icoChar = Unicode {_uniChar = '\61605'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [2,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "back"},Filter {_filtFilter = "finger"},Filter {_filtFilter = "left"},Filter {_filtFilter = "point"},Filter {_filtFilter = "previous"}]), _icoCategories = fromList [Category {_catCategory = "Directional Icons"},Category {_catCategory = "Hand Icons"}]}

fa_hand_o_up :: Icon
fa_hand_o_up = Icon {_icoName = IconName {_icnName = "Hand Outlined Up"}, _icoId = IconId {_iidId = "hand-o-up"}, _icoChar = Unicode {_uniChar = '\61606'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [2,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "finger"},Filter {_filtFilter = "point"}]), _icoCategories = fromList [Category {_catCategory = "Directional Icons"},Category {_catCategory = "Hand Icons"}]}

fa_hand_o_down :: Icon
fa_hand_o_down = Icon {_icoName = IconName {_icnName = "Hand Outlined Down"}, _icoId = IconId {_iidId = "hand-o-down"}, _icoChar = Unicode {_uniChar = '\61607'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [2,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "finger"},Filter {_filtFilter = "point"}]), _icoCategories = fromList [Category {_catCategory = "Directional Icons"},Category {_catCategory = "Hand Icons"}]}

fa_arrow_circle_left :: Icon
fa_arrow_circle_left = Icon {_icoName = IconName {_icnName = "Arrow Circle Left"}, _icoId = IconId {_iidId = "arrow-circle-left"}, _icoChar = Unicode {_uniChar = '\61608'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [2,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "back"},Filter {_filtFilter = "previous"}]), _icoCategories = fromList [Category {_catCategory = "Directional Icons"}]}

fa_arrow_circle_right :: Icon
fa_arrow_circle_right = Icon {_icoName = IconName {_icnName = "Arrow Circle Right"}, _icoId = IconId {_iidId = "arrow-circle-right"}, _icoChar = Unicode {_uniChar = '\61609'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [2,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "forward"},Filter {_filtFilter = "next"}]), _icoCategories = fromList [Category {_catCategory = "Directional Icons"}]}

fa_arrow_circle_up :: Icon
fa_arrow_circle_up = Icon {_icoName = IconName {_icnName = "Arrow Circle Up"}, _icoId = IconId {_iidId = "arrow-circle-up"}, _icoChar = Unicode {_uniChar = '\61610'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [2,0], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Directional Icons"}]}

fa_arrow_circle_down :: Icon
fa_arrow_circle_down = Icon {_icoName = IconName {_icnName = "Arrow Circle Down"}, _icoId = IconId {_iidId = "arrow-circle-down"}, _icoChar = Unicode {_uniChar = '\61611'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [2,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "download"}]), _icoCategories = fromList [Category {_catCategory = "Directional Icons"}]}

fa_globe :: Icon
fa_globe = Icon {_icoName = IconName {_icnName = "Globe"}, _icoId = IconId {_iidId = "globe"}, _icoChar = Unicode {_uniChar = '\61612'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [2,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "all"},Filter {_filtFilter = "coordinates"},Filter {_filtFilter = "country"},Filter {_filtFilter = "earth"},Filter {_filtFilter = "global"},Filter {_filtFilter = "language"},Filter {_filtFilter = "localize"},Filter {_filtFilter = "location"},Filter {_filtFilter = "map"},Filter {_filtFilter = "place"},Filter {_filtFilter = "planet"},Filter {_filtFilter = "translate"},Filter {_filtFilter = "travel"},Filter {_filtFilter = "world"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_wrench :: Icon
fa_wrench = Icon {_icoName = IconName {_icnName = "Wrench"}, _icoId = IconId {_iidId = "wrench"}, _icoChar = Unicode {_uniChar = '\61613'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [2,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "fix"},Filter {_filtFilter = "settings"},Filter {_filtFilter = "update"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_tasks :: Icon
fa_tasks = Icon {_icoName = IconName {_icnName = "Tasks"}, _icoId = IconId {_iidId = "tasks"}, _icoChar = Unicode {_uniChar = '\61614'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [2,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "downloading"},Filter {_filtFilter = "downloads"},Filter {_filtFilter = "loading"},Filter {_filtFilter = "progress"},Filter {_filtFilter = "settings"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_filter :: Icon
fa_filter = Icon {_icoName = IconName {_icnName = "Filter"}, _icoId = IconId {_iidId = "filter"}, _icoChar = Unicode {_uniChar = '\61616'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [2,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "funnel"},Filter {_filtFilter = "options"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_briefcase :: Icon
fa_briefcase = Icon {_icoName = IconName {_icnName = "Briefcase"}, _icoId = IconId {_iidId = "briefcase"}, _icoChar = Unicode {_uniChar = '\61617'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [2,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "bag"},Filter {_filtFilter = "business"},Filter {_filtFilter = "luggage"},Filter {_filtFilter = "office"},Filter {_filtFilter = "work"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_arrows_alt :: Icon
fa_arrows_alt = Icon {_icoName = IconName {_icnName = "Arrows Alt"}, _icoId = IconId {_iidId = "arrows-alt"}, _icoChar = Unicode {_uniChar = '\61618'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [2,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "arrow"},Filter {_filtFilter = "bigger"},Filter {_filtFilter = "enlarge"},Filter {_filtFilter = "expand"},Filter {_filtFilter = "fullscreen"},Filter {_filtFilter = "move"},Filter {_filtFilter = "reorder"},Filter {_filtFilter = "resize"}]), _icoCategories = fromList [Category {_catCategory = "Directional Icons"},Category {_catCategory = "Video Player Icons"}]}

fa_users :: Icon
fa_users = Icon {_icoName = IconName {_icnName = "Users"}, _icoId = IconId {_iidId = "users"}, _icoChar = Unicode {_uniChar = '\61632'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [2,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "people"},Filter {_filtFilter = "persons"},Filter {_filtFilter = "profiles"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_link :: Icon
fa_link = Icon {_icoName = IconName {_icnName = "Link"}, _icoId = IconId {_iidId = "link"}, _icoChar = Unicode {_uniChar = '\61633'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [2,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "chain"}]), _icoCategories = fromList [Category {_catCategory = "Text Editor Icons"}]}

fa_cloud :: Icon
fa_cloud = Icon {_icoName = IconName {_icnName = "Cloud"}, _icoId = IconId {_iidId = "cloud"}, _icoChar = Unicode {_uniChar = '\61634'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [2,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "save"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_flask :: Icon
fa_flask = Icon {_icoName = IconName {_icnName = "Flask"}, _icoId = IconId {_iidId = "flask"}, _icoChar = Unicode {_uniChar = '\61635'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [2,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "beaker"},Filter {_filtFilter = "experimental"},Filter {_filtFilter = "labs"},Filter {_filtFilter = "science"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_scissors :: Icon
fa_scissors = Icon {_icoName = IconName {_icnName = "Scissors"}, _icoId = IconId {_iidId = "scissors"}, _icoChar = Unicode {_uniChar = '\61636'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [2,0], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Text Editor Icons"}]}

fa_files_o :: Icon
fa_files_o = Icon {_icoName = IconName {_icnName = "Files Outlined"}, _icoId = IconId {_iidId = "files-o"}, _icoChar = Unicode {_uniChar = '\61637'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [2,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "clone"},Filter {_filtFilter = "copy"},Filter {_filtFilter = "duplicate"}]), _icoCategories = fromList [Category {_catCategory = "Text Editor Icons"}]}

fa_paperclip :: Icon
fa_paperclip = Icon {_icoName = IconName {_icnName = "Paperclip"}, _icoId = IconId {_iidId = "paperclip"}, _icoChar = Unicode {_uniChar = '\61638'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [2,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "attachment"}]), _icoCategories = fromList [Category {_catCategory = "Text Editor Icons"}]}

fa_floppy_o :: Icon
fa_floppy_o = Icon {_icoName = IconName {_icnName = "Floppy Outlined"}, _icoId = IconId {_iidId = "floppy-o"}, _icoChar = Unicode {_uniChar = '\61639'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [2,0], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Text Editor Icons"}]}

fa_square :: Icon
fa_square = Icon {_icoName = IconName {_icnName = "Square"}, _icoId = IconId {_iidId = "square"}, _icoChar = Unicode {_uniChar = '\61640'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [2,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "block"},Filter {_filtFilter = "box"}]), _icoCategories = fromList [Category {_catCategory = "Form Control Icons"},Category {_catCategory = "Web Application Icons"}]}

fa_bars :: Icon
fa_bars = Icon {_icoName = IconName {_icnName = "Bars"}, _icoId = IconId {_iidId = "bars"}, _icoChar = Unicode {_uniChar = '\61641'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [2,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "checklist"},Filter {_filtFilter = "drag"},Filter {_filtFilter = "hamburger"},Filter {_filtFilter = "list"},Filter {_filtFilter = "menu"},Filter {_filtFilter = "ol"},Filter {_filtFilter = "reorder"},Filter {_filtFilter = "settings"},Filter {_filtFilter = "todo"},Filter {_filtFilter = "ul"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_list_ul :: Icon
fa_list_ul = Icon {_icoName = IconName {_icnName = "list-ul"}, _icoId = IconId {_iidId = "list-ul"}, _icoChar = Unicode {_uniChar = '\61642'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [2,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "checklist"},Filter {_filtFilter = "list"},Filter {_filtFilter = "ol"},Filter {_filtFilter = "todo"},Filter {_filtFilter = "ul"}]), _icoCategories = fromList [Category {_catCategory = "Text Editor Icons"}]}

fa_list_ol :: Icon
fa_list_ol = Icon {_icoName = IconName {_icnName = "list-ol"}, _icoId = IconId {_iidId = "list-ol"}, _icoChar = Unicode {_uniChar = '\61643'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [2,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "checklist"},Filter {_filtFilter = "list"},Filter {_filtFilter = "numbers"},Filter {_filtFilter = "ol"},Filter {_filtFilter = "todo"},Filter {_filtFilter = "ul"}]), _icoCategories = fromList [Category {_catCategory = "Text Editor Icons"}]}

fa_strikethrough :: Icon
fa_strikethrough = Icon {_icoName = IconName {_icnName = "Strikethrough"}, _icoId = IconId {_iidId = "strikethrough"}, _icoChar = Unicode {_uniChar = '\61644'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [2,0], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Text Editor Icons"}]}

fa_underline :: Icon
fa_underline = Icon {_icoName = IconName {_icnName = "Underline"}, _icoId = IconId {_iidId = "underline"}, _icoChar = Unicode {_uniChar = '\61645'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [2,0], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Text Editor Icons"}]}

fa_table :: Icon
fa_table = Icon {_icoName = IconName {_icnName = "table"}, _icoId = IconId {_iidId = "table"}, _icoChar = Unicode {_uniChar = '\61646'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [2,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "data"},Filter {_filtFilter = "excel"},Filter {_filtFilter = "spreadsheet"}]), _icoCategories = fromList [Category {_catCategory = "Text Editor Icons"}]}

fa_magic :: Icon
fa_magic = Icon {_icoName = IconName {_icnName = "magic"}, _icoId = IconId {_iidId = "magic"}, _icoChar = Unicode {_uniChar = '\61648'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [2,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "autocomplete"},Filter {_filtFilter = "automatic"},Filter {_filtFilter = "wizard"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_truck :: Icon
fa_truck = Icon {_icoName = IconName {_icnName = "truck"}, _icoId = IconId {_iidId = "truck"}, _icoChar = Unicode {_uniChar = '\61649'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [2,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "shipping"}]), _icoCategories = fromList [Category {_catCategory = "Transportation Icons"},Category {_catCategory = "Web Application Icons"}]}

fa_pinterest :: Icon
fa_pinterest = Icon {_icoName = IconName {_icnName = "Pinterest"}, _icoId = IconId {_iidId = "pinterest"}, _icoChar = Unicode {_uniChar = '\61650'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [2,0], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_pinterest_square :: Icon
fa_pinterest_square = Icon {_icoName = IconName {_icnName = "Pinterest Square"}, _icoId = IconId {_iidId = "pinterest-square"}, _icoChar = Unicode {_uniChar = '\61651'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [2,0], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_google_plus_square :: Icon
fa_google_plus_square = Icon {_icoName = IconName {_icnName = "Google Plus Square"}, _icoId = IconId {_iidId = "google-plus-square"}, _icoChar = Unicode {_uniChar = '\61652'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [2,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "social network"}]), _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_google_plus :: Icon
fa_google_plus = Icon {_icoName = IconName {_icnName = "Google Plus"}, _icoId = IconId {_iidId = "google-plus"}, _icoChar = Unicode {_uniChar = '\61653'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [2,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "social network"}]), _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_money :: Icon
fa_money = Icon {_icoName = IconName {_icnName = "Money"}, _icoId = IconId {_iidId = "money"}, _icoChar = Unicode {_uniChar = '\61654'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [2,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "buy"},Filter {_filtFilter = "cash"},Filter {_filtFilter = "checkout"},Filter {_filtFilter = "money"},Filter {_filtFilter = "payment"},Filter {_filtFilter = "purchase"}]), _icoCategories = fromList [Category {_catCategory = "Currency Icons"},Category {_catCategory = "Web Application Icons"}]}

fa_caret_down :: Icon
fa_caret_down = Icon {_icoName = IconName {_icnName = "Caret Down"}, _icoId = IconId {_iidId = "caret-down"}, _icoChar = Unicode {_uniChar = '\61655'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [2,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "arrow"},Filter {_filtFilter = "dropdown"},Filter {_filtFilter = "menu"},Filter {_filtFilter = "more"},Filter {_filtFilter = "triangle down"}]), _icoCategories = fromList [Category {_catCategory = "Directional Icons"}]}

fa_caret_up :: Icon
fa_caret_up = Icon {_icoName = IconName {_icnName = "Caret Up"}, _icoId = IconId {_iidId = "caret-up"}, _icoChar = Unicode {_uniChar = '\61656'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [2,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "arrow"},Filter {_filtFilter = "triangle up"}]), _icoCategories = fromList [Category {_catCategory = "Directional Icons"}]}

fa_caret_left :: Icon
fa_caret_left = Icon {_icoName = IconName {_icnName = "Caret Left"}, _icoId = IconId {_iidId = "caret-left"}, _icoChar = Unicode {_uniChar = '\61657'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [2,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "arrow"},Filter {_filtFilter = "back"},Filter {_filtFilter = "previous"},Filter {_filtFilter = "triangle left"}]), _icoCategories = fromList [Category {_catCategory = "Directional Icons"}]}

fa_caret_right :: Icon
fa_caret_right = Icon {_icoName = IconName {_icnName = "Caret Right"}, _icoId = IconId {_iidId = "caret-right"}, _icoChar = Unicode {_uniChar = '\61658'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [2,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "arrow"},Filter {_filtFilter = "forward"},Filter {_filtFilter = "next"},Filter {_filtFilter = "triangle right"}]), _icoCategories = fromList [Category {_catCategory = "Directional Icons"}]}

fa_columns :: Icon
fa_columns = Icon {_icoName = IconName {_icnName = "Columns"}, _icoId = IconId {_iidId = "columns"}, _icoChar = Unicode {_uniChar = '\61659'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [2,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "panes"},Filter {_filtFilter = "split"}]), _icoCategories = fromList [Category {_catCategory = "Text Editor Icons"}]}

fa_sort :: Icon
fa_sort = Icon {_icoName = IconName {_icnName = "Sort"}, _icoId = IconId {_iidId = "sort"}, _icoChar = Unicode {_uniChar = '\61660'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [2,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "order"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_sort_desc :: Icon
fa_sort_desc = Icon {_icoName = IconName {_icnName = "Sort Descending"}, _icoId = IconId {_iidId = "sort-desc"}, _icoChar = Unicode {_uniChar = '\61661'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [2,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "arrow"},Filter {_filtFilter = "dropdown"},Filter {_filtFilter = "menu"},Filter {_filtFilter = "more"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_sort_asc :: Icon
fa_sort_asc = Icon {_icoName = IconName {_icnName = "Sort Ascending"}, _icoId = IconId {_iidId = "sort-asc"}, _icoChar = Unicode {_uniChar = '\61662'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [2,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "arrow"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_envelope :: Icon
fa_envelope = Icon {_icoName = IconName {_icnName = "Envelope"}, _icoId = IconId {_iidId = "envelope"}, _icoChar = Unicode {_uniChar = '\61664'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [2,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "e-mail"},Filter {_filtFilter = "email"},Filter {_filtFilter = "letter"},Filter {_filtFilter = "mail"},Filter {_filtFilter = "notification"},Filter {_filtFilter = "support"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_linkedin :: Icon
fa_linkedin = Icon {_icoName = IconName {_icnName = "LinkedIn"}, _icoId = IconId {_iidId = "linkedin"}, _icoChar = Unicode {_uniChar = '\61665'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [2,0], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_undo :: Icon
fa_undo = Icon {_icoName = IconName {_icnName = "Undo"}, _icoId = IconId {_iidId = "undo"}, _icoChar = Unicode {_uniChar = '\61666'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [2,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "back"}]), _icoCategories = fromList [Category {_catCategory = "Text Editor Icons"}]}

fa_gavel :: Icon
fa_gavel = Icon {_icoName = IconName {_icnName = "Gavel"}, _icoId = IconId {_iidId = "gavel"}, _icoChar = Unicode {_uniChar = '\61667'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [2,0], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_tachometer :: Icon
fa_tachometer = Icon {_icoName = IconName {_icnName = "Tachometer"}, _icoId = IconId {_iidId = "tachometer"}, _icoChar = Unicode {_uniChar = '\61668'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [2,0], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_comment_o :: Icon
fa_comment_o = Icon {_icoName = IconName {_icnName = "comment-o"}, _icoId = IconId {_iidId = "comment-o"}, _icoChar = Unicode {_uniChar = '\61669'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [2,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "bubble"},Filter {_filtFilter = "chat"},Filter {_filtFilter = "feedback"},Filter {_filtFilter = "message"},Filter {_filtFilter = "note"},Filter {_filtFilter = "notification"},Filter {_filtFilter = "sms"},Filter {_filtFilter = "speech"},Filter {_filtFilter = "texting"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_comments_o :: Icon
fa_comments_o = Icon {_icoName = IconName {_icnName = "comments-o"}, _icoId = IconId {_iidId = "comments-o"}, _icoChar = Unicode {_uniChar = '\61670'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [2,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "chat"},Filter {_filtFilter = "conversation"},Filter {_filtFilter = "message"},Filter {_filtFilter = "notes"},Filter {_filtFilter = "notification"},Filter {_filtFilter = "sms"},Filter {_filtFilter = "texting"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_bolt :: Icon
fa_bolt = Icon {_icoName = IconName {_icnName = "Lightning Bolt"}, _icoId = IconId {_iidId = "bolt"}, _icoChar = Unicode {_uniChar = '\61671'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [2,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "lightning"},Filter {_filtFilter = "weather"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_sitemap :: Icon
fa_sitemap = Icon {_icoName = IconName {_icnName = "Sitemap"}, _icoId = IconId {_iidId = "sitemap"}, _icoChar = Unicode {_uniChar = '\61672'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [2,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "directory"},Filter {_filtFilter = "hierarchy"},Filter {_filtFilter = "organization"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_umbrella :: Icon
fa_umbrella = Icon {_icoName = IconName {_icnName = "Umbrella"}, _icoId = IconId {_iidId = "umbrella"}, _icoChar = Unicode {_uniChar = '\61673'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [2,0], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_clipboard :: Icon
fa_clipboard = Icon {_icoName = IconName {_icnName = "Clipboard"}, _icoId = IconId {_iidId = "clipboard"}, _icoChar = Unicode {_uniChar = '\61674'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [2,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "copy"}]), _icoCategories = fromList [Category {_catCategory = "Text Editor Icons"}]}

fa_lightbulb_o :: Icon
fa_lightbulb_o = Icon {_icoName = IconName {_icnName = "Lightbulb Outlined"}, _icoId = IconId {_iidId = "lightbulb-o"}, _icoChar = Unicode {_uniChar = '\61675'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "idea"},Filter {_filtFilter = "inspiration"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_exchange :: Icon
fa_exchange = Icon {_icoName = IconName {_icnName = "Exchange"}, _icoId = IconId {_iidId = "exchange"}, _icoChar = Unicode {_uniChar = '\61676'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "arrow"},Filter {_filtFilter = "arrows"},Filter {_filtFilter = "transfer"}]), _icoCategories = fromList [Category {_catCategory = "Directional Icons"},Category {_catCategory = "Web Application Icons"}]}

fa_cloud_download :: Icon
fa_cloud_download = Icon {_icoName = IconName {_icnName = "Cloud Download"}, _icoId = IconId {_iidId = "cloud-download"}, _icoChar = Unicode {_uniChar = '\61677'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "import"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_cloud_upload :: Icon
fa_cloud_upload = Icon {_icoName = IconName {_icnName = "Cloud Upload"}, _icoId = IconId {_iidId = "cloud-upload"}, _icoChar = Unicode {_uniChar = '\61678'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "import"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_user_md :: Icon
fa_user_md = Icon {_icoName = IconName {_icnName = "user-md"}, _icoId = IconId {_iidId = "user-md"}, _icoChar = Unicode {_uniChar = '\61680'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [2,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "doctor"},Filter {_filtFilter = "medical"},Filter {_filtFilter = "nurse"},Filter {_filtFilter = "profile"}]), _icoCategories = fromList [Category {_catCategory = "Medical Icons"}]}

fa_stethoscope :: Icon
fa_stethoscope = Icon {_icoName = IconName {_icnName = "Stethoscope"}, _icoId = IconId {_iidId = "stethoscope"}, _icoChar = Unicode {_uniChar = '\61681'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,0], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Medical Icons"}]}

fa_suitcase :: Icon
fa_suitcase = Icon {_icoName = IconName {_icnName = "Suitcase"}, _icoId = IconId {_iidId = "suitcase"}, _icoChar = Unicode {_uniChar = '\61682'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "baggage"},Filter {_filtFilter = "luggage"},Filter {_filtFilter = "move"},Filter {_filtFilter = "travel"},Filter {_filtFilter = "trip"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_bell_o :: Icon
fa_bell_o = Icon {_icoName = IconName {_icnName = "Bell Outlined"}, _icoId = IconId {_iidId = "bell-o"}, _icoChar = Unicode {_uniChar = '\61602'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "alert"},Filter {_filtFilter = "notification"},Filter {_filtFilter = "reminder"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_coffee :: Icon
fa_coffee = Icon {_icoName = IconName {_icnName = "Coffee"}, _icoId = IconId {_iidId = "coffee"}, _icoChar = Unicode {_uniChar = '\61684'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "breakfast"},Filter {_filtFilter = "cafe"},Filter {_filtFilter = "drink"},Filter {_filtFilter = "morning"},Filter {_filtFilter = "mug"},Filter {_filtFilter = "tea"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_cutlery :: Icon
fa_cutlery = Icon {_icoName = IconName {_icnName = "Cutlery"}, _icoId = IconId {_iidId = "cutlery"}, _icoChar = Unicode {_uniChar = '\61685'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "dinner"},Filter {_filtFilter = "eat"},Filter {_filtFilter = "food"},Filter {_filtFilter = "knife"},Filter {_filtFilter = "restaurant"},Filter {_filtFilter = "spoon"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_file_text_o :: Icon
fa_file_text_o = Icon {_icoName = IconName {_icnName = "File Text Outlined"}, _icoId = IconId {_iidId = "file-text-o"}, _icoChar = Unicode {_uniChar = '\61686'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "document"},Filter {_filtFilter = "new"},Filter {_filtFilter = "page"},Filter {_filtFilter = "pdf"}]), _icoCategories = fromList [Category {_catCategory = "File Type Icons"},Category {_catCategory = "Text Editor Icons"}]}

fa_building_o :: Icon
fa_building_o = Icon {_icoName = IconName {_icnName = "Building Outlined"}, _icoId = IconId {_iidId = "building-o"}, _icoChar = Unicode {_uniChar = '\61687'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "apartment"},Filter {_filtFilter = "business"},Filter {_filtFilter = "company"},Filter {_filtFilter = "office"},Filter {_filtFilter = "work"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_hospital_o :: Icon
fa_hospital_o = Icon {_icoName = IconName {_icnName = "hospital Outlined"}, _icoId = IconId {_iidId = "hospital-o"}, _icoChar = Unicode {_uniChar = '\61688'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "building"}]), _icoCategories = fromList [Category {_catCategory = "Medical Icons"}]}

fa_ambulance :: Icon
fa_ambulance = Icon {_icoName = IconName {_icnName = "ambulance"}, _icoId = IconId {_iidId = "ambulance"}, _icoChar = Unicode {_uniChar = '\61689'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "help"},Filter {_filtFilter = "support"},Filter {_filtFilter = "vehicle"}]), _icoCategories = fromList [Category {_catCategory = "Medical Icons"},Category {_catCategory = "Transportation Icons"}]}

fa_medkit :: Icon
fa_medkit = Icon {_icoName = IconName {_icnName = "medkit"}, _icoId = IconId {_iidId = "medkit"}, _icoChar = Unicode {_uniChar = '\61690'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "first aid"},Filter {_filtFilter = "firstaid"},Filter {_filtFilter = "health"},Filter {_filtFilter = "help"},Filter {_filtFilter = "support"}]), _icoCategories = fromList [Category {_catCategory = "Medical Icons"}]}

fa_fighter_jet :: Icon
fa_fighter_jet = Icon {_icoName = IconName {_icnName = "fighter-jet"}, _icoId = IconId {_iidId = "fighter-jet"}, _icoChar = Unicode {_uniChar = '\61691'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "airplane"},Filter {_filtFilter = "fast"},Filter {_filtFilter = "fly"},Filter {_filtFilter = "plane"},Filter {_filtFilter = "quick"},Filter {_filtFilter = "travel"}]), _icoCategories = fromList [Category {_catCategory = "Transportation Icons"},Category {_catCategory = "Web Application Icons"}]}

fa_beer :: Icon
fa_beer = Icon {_icoName = IconName {_icnName = "beer"}, _icoId = IconId {_iidId = "beer"}, _icoChar = Unicode {_uniChar = '\61692'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "alcohol"},Filter {_filtFilter = "bar"},Filter {_filtFilter = "drink"},Filter {_filtFilter = "liquor"},Filter {_filtFilter = "mug"},Filter {_filtFilter = "stein"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_h_square :: Icon
fa_h_square = Icon {_icoName = IconName {_icnName = "H Square"}, _icoId = IconId {_iidId = "h-square"}, _icoChar = Unicode {_uniChar = '\61693'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "hospital"},Filter {_filtFilter = "hotel"}]), _icoCategories = fromList [Category {_catCategory = "Medical Icons"}]}

fa_plus_square :: Icon
fa_plus_square = Icon {_icoName = IconName {_icnName = "Plus Square"}, _icoId = IconId {_iidId = "plus-square"}, _icoChar = Unicode {_uniChar = '\61694'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "add"},Filter {_filtFilter = "create"},Filter {_filtFilter = "expand"},Filter {_filtFilter = "new"}]), _icoCategories = fromList [Category {_catCategory = "Form Control Icons"},Category {_catCategory = "Medical Icons"},Category {_catCategory = "Web Application Icons"}]}

fa_angle_double_left :: Icon
fa_angle_double_left = Icon {_icoName = IconName {_icnName = "Angle Double Left"}, _icoId = IconId {_iidId = "angle-double-left"}, _icoChar = Unicode {_uniChar = '\61696'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "arrows"},Filter {_filtFilter = "back"},Filter {_filtFilter = "laquo"},Filter {_filtFilter = "previous"},Filter {_filtFilter = "quote"}]), _icoCategories = fromList [Category {_catCategory = "Directional Icons"}]}

fa_angle_double_right :: Icon
fa_angle_double_right = Icon {_icoName = IconName {_icnName = "Angle Double Right"}, _icoId = IconId {_iidId = "angle-double-right"}, _icoChar = Unicode {_uniChar = '\61697'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "arrows"},Filter {_filtFilter = "forward"},Filter {_filtFilter = "next"},Filter {_filtFilter = "quote"},Filter {_filtFilter = "raquo"}]), _icoCategories = fromList [Category {_catCategory = "Directional Icons"}]}

fa_angle_double_up :: Icon
fa_angle_double_up = Icon {_icoName = IconName {_icnName = "Angle Double Up"}, _icoId = IconId {_iidId = "angle-double-up"}, _icoChar = Unicode {_uniChar = '\61698'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "arrows"}]), _icoCategories = fromList [Category {_catCategory = "Directional Icons"}]}

fa_angle_double_down :: Icon
fa_angle_double_down = Icon {_icoName = IconName {_icnName = "Angle Double Down"}, _icoId = IconId {_iidId = "angle-double-down"}, _icoChar = Unicode {_uniChar = '\61699'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "arrows"}]), _icoCategories = fromList [Category {_catCategory = "Directional Icons"}]}

fa_angle_left :: Icon
fa_angle_left = Icon {_icoName = IconName {_icnName = "angle-left"}, _icoId = IconId {_iidId = "angle-left"}, _icoChar = Unicode {_uniChar = '\61700'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "arrow"},Filter {_filtFilter = "back"},Filter {_filtFilter = "previous"}]), _icoCategories = fromList [Category {_catCategory = "Directional Icons"}]}

fa_angle_right :: Icon
fa_angle_right = Icon {_icoName = IconName {_icnName = "angle-right"}, _icoId = IconId {_iidId = "angle-right"}, _icoChar = Unicode {_uniChar = '\61701'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "arrow"},Filter {_filtFilter = "forward"},Filter {_filtFilter = "next"}]), _icoCategories = fromList [Category {_catCategory = "Directional Icons"}]}

fa_angle_up :: Icon
fa_angle_up = Icon {_icoName = IconName {_icnName = "angle-up"}, _icoId = IconId {_iidId = "angle-up"}, _icoChar = Unicode {_uniChar = '\61702'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "arrow"}]), _icoCategories = fromList [Category {_catCategory = "Directional Icons"}]}

fa_angle_down :: Icon
fa_angle_down = Icon {_icoName = IconName {_icnName = "angle-down"}, _icoId = IconId {_iidId = "angle-down"}, _icoChar = Unicode {_uniChar = '\61703'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "arrow"}]), _icoCategories = fromList [Category {_catCategory = "Directional Icons"}]}

fa_desktop :: Icon
fa_desktop = Icon {_icoName = IconName {_icnName = "Desktop"}, _icoId = IconId {_iidId = "desktop"}, _icoChar = Unicode {_uniChar = '\61704'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "computer"},Filter {_filtFilter = "demo"},Filter {_filtFilter = "desktop"},Filter {_filtFilter = "device"},Filter {_filtFilter = "monitor"},Filter {_filtFilter = "screen"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_laptop :: Icon
fa_laptop = Icon {_icoName = IconName {_icnName = "Laptop"}, _icoId = IconId {_iidId = "laptop"}, _icoChar = Unicode {_uniChar = '\61705'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "computer"},Filter {_filtFilter = "demo"},Filter {_filtFilter = "device"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_tablet :: Icon
fa_tablet = Icon {_icoName = IconName {_icnName = "tablet"}, _icoId = IconId {_iidId = "tablet"}, _icoChar = Unicode {_uniChar = '\61706'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "device"},Filter {_filtFilter = "ipad"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_mobile :: Icon
fa_mobile = Icon {_icoName = IconName {_icnName = "Mobile Phone"}, _icoId = IconId {_iidId = "mobile"}, _icoChar = Unicode {_uniChar = '\61707'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "call"},Filter {_filtFilter = "cell phone"},Filter {_filtFilter = "cellphone"},Filter {_filtFilter = "iphone"},Filter {_filtFilter = "number"},Filter {_filtFilter = "text"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_circle_o :: Icon
fa_circle_o = Icon {_icoName = IconName {_icnName = "Circle Outlined"}, _icoId = IconId {_iidId = "circle-o"}, _icoChar = Unicode {_uniChar = '\61708'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,0], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Form Control Icons"},Category {_catCategory = "Web Application Icons"}]}

fa_quote_left :: Icon
fa_quote_left = Icon {_icoName = IconName {_icnName = "quote-left"}, _icoId = IconId {_iidId = "quote-left"}, _icoChar = Unicode {_uniChar = '\61709'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,0], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_quote_right :: Icon
fa_quote_right = Icon {_icoName = IconName {_icnName = "quote-right"}, _icoId = IconId {_iidId = "quote-right"}, _icoChar = Unicode {_uniChar = '\61710'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,0], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_spinner :: Icon
fa_spinner = Icon {_icoName = IconName {_icnName = "Spinner"}, _icoId = IconId {_iidId = "spinner"}, _icoChar = Unicode {_uniChar = '\61712'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "loading"},Filter {_filtFilter = "progress"}]), _icoCategories = fromList [Category {_catCategory = "Spinner Icons"},Category {_catCategory = "Web Application Icons"}]}

fa_circle :: Icon
fa_circle = Icon {_icoName = IconName {_icnName = "Circle"}, _icoId = IconId {_iidId = "circle"}, _icoChar = Unicode {_uniChar = '\61713'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "dot"},Filter {_filtFilter = "notification"}]), _icoCategories = fromList [Category {_catCategory = "Form Control Icons"},Category {_catCategory = "Web Application Icons"}]}

fa_reply :: Icon
fa_reply = Icon {_icoName = IconName {_icnName = "Reply"}, _icoId = IconId {_iidId = "reply"}, _icoChar = Unicode {_uniChar = '\61714'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,0], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_github_alt :: Icon
fa_github_alt = Icon {_icoName = IconName {_icnName = "GitHub Alt"}, _icoId = IconId {_iidId = "github-alt"}, _icoChar = Unicode {_uniChar = '\61715'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "octocat"}]), _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_folder_o :: Icon
fa_folder_o = Icon {_icoName = IconName {_icnName = "Folder Outlined"}, _icoId = IconId {_iidId = "folder-o"}, _icoChar = Unicode {_uniChar = '\61716'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,0], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_folder_open_o :: Icon
fa_folder_open_o = Icon {_icoName = IconName {_icnName = "Folder Open Outlined"}, _icoId = IconId {_iidId = "folder-open-o"}, _icoChar = Unicode {_uniChar = '\61717'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,0], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_smile_o :: Icon
fa_smile_o = Icon {_icoName = IconName {_icnName = "Smile Outlined"}, _icoId = IconId {_iidId = "smile-o"}, _icoChar = Unicode {_uniChar = '\61720'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,1], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "approve"},Filter {_filtFilter = "emoticon"},Filter {_filtFilter = "face"},Filter {_filtFilter = "happy"},Filter {_filtFilter = "rating"},Filter {_filtFilter = "satisfied"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_frown_o :: Icon
fa_frown_o = Icon {_icoName = IconName {_icnName = "Frown Outlined"}, _icoId = IconId {_iidId = "frown-o"}, _icoChar = Unicode {_uniChar = '\61721'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,1], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "disapprove"},Filter {_filtFilter = "emoticon"},Filter {_filtFilter = "face"},Filter {_filtFilter = "rating"},Filter {_filtFilter = "sad"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_meh_o :: Icon
fa_meh_o = Icon {_icoName = IconName {_icnName = "Meh Outlined"}, _icoId = IconId {_iidId = "meh-o"}, _icoChar = Unicode {_uniChar = '\61722'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,1], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "emoticon"},Filter {_filtFilter = "face"},Filter {_filtFilter = "neutral"},Filter {_filtFilter = "rating"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_gamepad :: Icon
fa_gamepad = Icon {_icoName = IconName {_icnName = "Gamepad"}, _icoId = IconId {_iidId = "gamepad"}, _icoChar = Unicode {_uniChar = '\61723'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,1], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "controller"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_keyboard_o :: Icon
fa_keyboard_o = Icon {_icoName = IconName {_icnName = "Keyboard Outlined"}, _icoId = IconId {_iidId = "keyboard-o"}, _icoChar = Unicode {_uniChar = '\61724'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,1], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "input"},Filter {_filtFilter = "type"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_flag_o :: Icon
fa_flag_o = Icon {_icoName = IconName {_icnName = "Flag Outlined"}, _icoId = IconId {_iidId = "flag-o"}, _icoChar = Unicode {_uniChar = '\61725'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,1], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "notification"},Filter {_filtFilter = "report"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_flag_checkered :: Icon
fa_flag_checkered = Icon {_icoName = IconName {_icnName = "flag-checkered"}, _icoId = IconId {_iidId = "flag-checkered"}, _icoChar = Unicode {_uniChar = '\61726'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,1], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "notification"},Filter {_filtFilter = "notify"},Filter {_filtFilter = "report"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_terminal :: Icon
fa_terminal = Icon {_icoName = IconName {_icnName = "Terminal"}, _icoId = IconId {_iidId = "terminal"}, _icoChar = Unicode {_uniChar = '\61728'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,1], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "code"},Filter {_filtFilter = "command"},Filter {_filtFilter = "prompt"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_code :: Icon
fa_code = Icon {_icoName = IconName {_icnName = "Code"}, _icoId = IconId {_iidId = "code"}, _icoChar = Unicode {_uniChar = '\61729'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,1], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "brackets"},Filter {_filtFilter = "html"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_reply_all :: Icon
fa_reply_all = Icon {_icoName = IconName {_icnName = "reply-all"}, _icoId = IconId {_iidId = "reply-all"}, _icoChar = Unicode {_uniChar = '\61730'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,1], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_star_half_o :: Icon
fa_star_half_o = Icon {_icoName = IconName {_icnName = "Star Half Outlined"}, _icoId = IconId {_iidId = "star-half-o"}, _icoChar = Unicode {_uniChar = '\61731'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,1], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "achievement"},Filter {_filtFilter = "award"},Filter {_filtFilter = "rating"},Filter {_filtFilter = "score"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_location_arrow :: Icon
fa_location_arrow = Icon {_icoName = IconName {_icnName = "location-arrow"}, _icoId = IconId {_iidId = "location-arrow"}, _icoChar = Unicode {_uniChar = '\61732'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,1], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "address"},Filter {_filtFilter = "coordinates"},Filter {_filtFilter = "location"},Filter {_filtFilter = "map"},Filter {_filtFilter = "place"},Filter {_filtFilter = "where"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_crop :: Icon
fa_crop = Icon {_icoName = IconName {_icnName = "crop"}, _icoId = IconId {_iidId = "crop"}, _icoChar = Unicode {_uniChar = '\61733'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,1], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_code_fork :: Icon
fa_code_fork = Icon {_icoName = IconName {_icnName = "code-fork"}, _icoId = IconId {_iidId = "code-fork"}, _icoChar = Unicode {_uniChar = '\61734'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,1], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "fork"},Filter {_filtFilter = "git"},Filter {_filtFilter = "github"},Filter {_filtFilter = "merge"},Filter {_filtFilter = "rebase"},Filter {_filtFilter = "svn"},Filter {_filtFilter = "vcs"},Filter {_filtFilter = "version"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_chain_broken :: Icon
fa_chain_broken = Icon {_icoName = IconName {_icnName = "Chain Broken"}, _icoId = IconId {_iidId = "chain-broken"}, _icoChar = Unicode {_uniChar = '\61735'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,1], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "remove"}]), _icoCategories = fromList [Category {_catCategory = "Text Editor Icons"}]}

fa_question :: Icon
fa_question = Icon {_icoName = IconName {_icnName = "Question"}, _icoId = IconId {_iidId = "question"}, _icoChar = Unicode {_uniChar = '\61736'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,1], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "help"},Filter {_filtFilter = "information"},Filter {_filtFilter = "support"},Filter {_filtFilter = "unknown"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_info :: Icon
fa_info = Icon {_icoName = IconName {_icnName = "Info"}, _icoId = IconId {_iidId = "info"}, _icoChar = Unicode {_uniChar = '\61737'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,1], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "details"},Filter {_filtFilter = "help"},Filter {_filtFilter = "information"},Filter {_filtFilter = "more"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_exclamation :: Icon
fa_exclamation = Icon {_icoName = IconName {_icnName = "exclamation"}, _icoId = IconId {_iidId = "exclamation"}, _icoChar = Unicode {_uniChar = '\61738'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,1], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "alert"},Filter {_filtFilter = "error"},Filter {_filtFilter = "notification"},Filter {_filtFilter = "notify"},Filter {_filtFilter = "problem"},Filter {_filtFilter = "warning"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_superscript :: Icon
fa_superscript = Icon {_icoName = IconName {_icnName = "superscript"}, _icoId = IconId {_iidId = "superscript"}, _icoChar = Unicode {_uniChar = '\61739'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,1], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "exponential"}]), _icoCategories = fromList [Category {_catCategory = "Text Editor Icons"}]}

fa_subscript :: Icon
fa_subscript = Icon {_icoName = IconName {_icnName = "subscript"}, _icoId = IconId {_iidId = "subscript"}, _icoChar = Unicode {_uniChar = '\61740'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,1], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Text Editor Icons"}]}

fa_eraser :: Icon
fa_eraser = Icon {_icoName = IconName {_icnName = "eraser"}, _icoId = IconId {_iidId = "eraser"}, _icoChar = Unicode {_uniChar = '\61741'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,1], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "delete"},Filter {_filtFilter = "remove"}]), _icoCategories = fromList [Category {_catCategory = "Text Editor Icons"},Category {_catCategory = "Web Application Icons"}]}

fa_puzzle_piece :: Icon
fa_puzzle_piece = Icon {_icoName = IconName {_icnName = "Puzzle Piece"}, _icoId = IconId {_iidId = "puzzle-piece"}, _icoChar = Unicode {_uniChar = '\61742'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,1], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "add-on"},Filter {_filtFilter = "addon"},Filter {_filtFilter = "section"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_microphone :: Icon
fa_microphone = Icon {_icoName = IconName {_icnName = "microphone"}, _icoId = IconId {_iidId = "microphone"}, _icoChar = Unicode {_uniChar = '\61744'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,1], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "record"},Filter {_filtFilter = "sound"},Filter {_filtFilter = "voice"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_microphone_slash :: Icon
fa_microphone_slash = Icon {_icoName = IconName {_icnName = "Microphone Slash"}, _icoId = IconId {_iidId = "microphone-slash"}, _icoChar = Unicode {_uniChar = '\61745'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,1], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "mute"},Filter {_filtFilter = "record"},Filter {_filtFilter = "sound"},Filter {_filtFilter = "voice"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_shield :: Icon
fa_shield = Icon {_icoName = IconName {_icnName = "shield"}, _icoId = IconId {_iidId = "shield"}, _icoChar = Unicode {_uniChar = '\61746'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,1], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "achievement"},Filter {_filtFilter = "award"},Filter {_filtFilter = "winner"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_calendar_o :: Icon
fa_calendar_o = Icon {_icoName = IconName {_icnName = "calendar-o"}, _icoId = IconId {_iidId = "calendar-o"}, _icoChar = Unicode {_uniChar = '\61747'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,1], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "date"},Filter {_filtFilter = "event"},Filter {_filtFilter = "time"},Filter {_filtFilter = "when"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_fire_extinguisher :: Icon
fa_fire_extinguisher = Icon {_icoName = IconName {_icnName = "fire-extinguisher"}, _icoId = IconId {_iidId = "fire-extinguisher"}, _icoChar = Unicode {_uniChar = '\61748'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,1], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_rocket :: Icon
fa_rocket = Icon {_icoName = IconName {_icnName = "rocket"}, _icoId = IconId {_iidId = "rocket"}, _icoChar = Unicode {_uniChar = '\61749'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,1], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "app"}]), _icoCategories = fromList [Category {_catCategory = "Transportation Icons"},Category {_catCategory = "Web Application Icons"}]}

fa_maxcdn :: Icon
fa_maxcdn = Icon {_icoName = IconName {_icnName = "MaxCDN"}, _icoId = IconId {_iidId = "maxcdn"}, _icoChar = Unicode {_uniChar = '\61750'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,1], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_chevron_circle_left :: Icon
fa_chevron_circle_left = Icon {_icoName = IconName {_icnName = "Chevron Circle Left"}, _icoId = IconId {_iidId = "chevron-circle-left"}, _icoChar = Unicode {_uniChar = '\61751'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,1], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "arrow"},Filter {_filtFilter = "back"},Filter {_filtFilter = "previous"}]), _icoCategories = fromList [Category {_catCategory = "Directional Icons"}]}

fa_chevron_circle_right :: Icon
fa_chevron_circle_right = Icon {_icoName = IconName {_icnName = "Chevron Circle Right"}, _icoId = IconId {_iidId = "chevron-circle-right"}, _icoChar = Unicode {_uniChar = '\61752'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,1], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "arrow"},Filter {_filtFilter = "forward"},Filter {_filtFilter = "next"}]), _icoCategories = fromList [Category {_catCategory = "Directional Icons"}]}

fa_chevron_circle_up :: Icon
fa_chevron_circle_up = Icon {_icoName = IconName {_icnName = "Chevron Circle Up"}, _icoId = IconId {_iidId = "chevron-circle-up"}, _icoChar = Unicode {_uniChar = '\61753'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,1], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "arrow"}]), _icoCategories = fromList [Category {_catCategory = "Directional Icons"}]}

fa_chevron_circle_down :: Icon
fa_chevron_circle_down = Icon {_icoName = IconName {_icnName = "Chevron Circle Down"}, _icoId = IconId {_iidId = "chevron-circle-down"}, _icoChar = Unicode {_uniChar = '\61754'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,1], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "arrow"},Filter {_filtFilter = "dropdown"},Filter {_filtFilter = "menu"},Filter {_filtFilter = "more"}]), _icoCategories = fromList [Category {_catCategory = "Directional Icons"}]}

fa_html5 :: Icon
fa_html5 = Icon {_icoName = IconName {_icnName = "HTML 5 Logo"}, _icoId = IconId {_iidId = "html5"}, _icoChar = Unicode {_uniChar = '\61755'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,1], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_css3 :: Icon
fa_css3 = Icon {_icoName = IconName {_icnName = "CSS 3 Logo"}, _icoId = IconId {_iidId = "css3"}, _icoChar = Unicode {_uniChar = '\61756'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,1], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "code"}]), _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_anchor :: Icon
fa_anchor = Icon {_icoName = IconName {_icnName = "Anchor"}, _icoId = IconId {_iidId = "anchor"}, _icoChar = Unicode {_uniChar = '\61757'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,1], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "link"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_unlock_alt :: Icon
fa_unlock_alt = Icon {_icoName = IconName {_icnName = "Unlock Alt"}, _icoId = IconId {_iidId = "unlock-alt"}, _icoChar = Unicode {_uniChar = '\61758'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,1], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "admin"},Filter {_filtFilter = "lock"},Filter {_filtFilter = "password"},Filter {_filtFilter = "protect"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_bullseye :: Icon
fa_bullseye = Icon {_icoName = IconName {_icnName = "Bullseye"}, _icoId = IconId {_iidId = "bullseye"}, _icoChar = Unicode {_uniChar = '\61760'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,1], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "target"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_ellipsis_h :: Icon
fa_ellipsis_h = Icon {_icoName = IconName {_icnName = "Ellipsis Horizontal"}, _icoId = IconId {_iidId = "ellipsis-h"}, _icoChar = Unicode {_uniChar = '\61761'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,1], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "dots"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_ellipsis_v :: Icon
fa_ellipsis_v = Icon {_icoName = IconName {_icnName = "Ellipsis Vertical"}, _icoId = IconId {_iidId = "ellipsis-v"}, _icoChar = Unicode {_uniChar = '\61762'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,1], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "dots"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_rss_square :: Icon
fa_rss_square = Icon {_icoName = IconName {_icnName = "RSS Square"}, _icoId = IconId {_iidId = "rss-square"}, _icoChar = Unicode {_uniChar = '\61763'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,1], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "blog"},Filter {_filtFilter = "feed"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_play_circle :: Icon
fa_play_circle = Icon {_icoName = IconName {_icnName = "Play Circle"}, _icoId = IconId {_iidId = "play-circle"}, _icoChar = Unicode {_uniChar = '\61764'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,1], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "playing"},Filter {_filtFilter = "start"}]), _icoCategories = fromList [Category {_catCategory = "Video Player Icons"}]}

fa_ticket :: Icon
fa_ticket = Icon {_icoName = IconName {_icnName = "Ticket"}, _icoId = IconId {_iidId = "ticket"}, _icoChar = Unicode {_uniChar = '\61765'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,1], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "movie"},Filter {_filtFilter = "pass"},Filter {_filtFilter = "support"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_minus_square :: Icon
fa_minus_square = Icon {_icoName = IconName {_icnName = "Minus Square"}, _icoId = IconId {_iidId = "minus-square"}, _icoChar = Unicode {_uniChar = '\61766'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,1], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "collapse"},Filter {_filtFilter = "delete"},Filter {_filtFilter = "hide"},Filter {_filtFilter = "minify"},Filter {_filtFilter = "remove"},Filter {_filtFilter = "trash"}]), _icoCategories = fromList [Category {_catCategory = "Form Control Icons"},Category {_catCategory = "Web Application Icons"}]}

fa_minus_square_o :: Icon
fa_minus_square_o = Icon {_icoName = IconName {_icnName = "Minus Square Outlined"}, _icoId = IconId {_iidId = "minus-square-o"}, _icoChar = Unicode {_uniChar = '\61767'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,1], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "collapse"},Filter {_filtFilter = "delete"},Filter {_filtFilter = "hide"},Filter {_filtFilter = "minify"},Filter {_filtFilter = "remove"},Filter {_filtFilter = "trash"}]), _icoCategories = fromList [Category {_catCategory = "Form Control Icons"},Category {_catCategory = "Web Application Icons"}]}

fa_level_up :: Icon
fa_level_up = Icon {_icoName = IconName {_icnName = "Level Up"}, _icoId = IconId {_iidId = "level-up"}, _icoChar = Unicode {_uniChar = '\61768'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,1], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "arrow"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_level_down :: Icon
fa_level_down = Icon {_icoName = IconName {_icnName = "Level Down"}, _icoId = IconId {_iidId = "level-down"}, _icoChar = Unicode {_uniChar = '\61769'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,1], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "arrow"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_check_square :: Icon
fa_check_square = Icon {_icoName = IconName {_icnName = "Check Square"}, _icoId = IconId {_iidId = "check-square"}, _icoChar = Unicode {_uniChar = '\61770'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,1], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "accept"},Filter {_filtFilter = "agree"},Filter {_filtFilter = "checkmark"},Filter {_filtFilter = "confirm"},Filter {_filtFilter = "done"},Filter {_filtFilter = "ok"},Filter {_filtFilter = "todo"}]), _icoCategories = fromList [Category {_catCategory = "Form Control Icons"},Category {_catCategory = "Web Application Icons"}]}

fa_pencil_square :: Icon
fa_pencil_square = Icon {_icoName = IconName {_icnName = "Pencil Square"}, _icoId = IconId {_iidId = "pencil-square"}, _icoChar = Unicode {_uniChar = '\61771'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,1], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "edit"},Filter {_filtFilter = "update"},Filter {_filtFilter = "write"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_external_link_square :: Icon
fa_external_link_square = Icon {_icoName = IconName {_icnName = "External Link Square"}, _icoId = IconId {_iidId = "external-link-square"}, _icoChar = Unicode {_uniChar = '\61772'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,1], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "new"},Filter {_filtFilter = "open"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_share_square :: Icon
fa_share_square = Icon {_icoName = IconName {_icnName = "Share Square"}, _icoId = IconId {_iidId = "share-square"}, _icoChar = Unicode {_uniChar = '\61773'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,1], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "send"},Filter {_filtFilter = "social"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_compass :: Icon
fa_compass = Icon {_icoName = IconName {_icnName = "Compass"}, _icoId = IconId {_iidId = "compass"}, _icoChar = Unicode {_uniChar = '\61774'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,2], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "directory"},Filter {_filtFilter = "location"},Filter {_filtFilter = "menu"},Filter {_filtFilter = "safari"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_caret_square_o_down :: Icon
fa_caret_square_o_down = Icon {_icoName = IconName {_icnName = "Caret Square Outlined Down"}, _icoId = IconId {_iidId = "caret-square-o-down"}, _icoChar = Unicode {_uniChar = '\61776'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,2], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "dropdown"},Filter {_filtFilter = "menu"},Filter {_filtFilter = "more"}]), _icoCategories = fromList [Category {_catCategory = "Directional Icons"},Category {_catCategory = "Web Application Icons"}]}

fa_caret_square_o_up :: Icon
fa_caret_square_o_up = Icon {_icoName = IconName {_icnName = "Caret Square Outlined Up"}, _icoId = IconId {_iidId = "caret-square-o-up"}, _icoChar = Unicode {_uniChar = '\61777'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,2], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Directional Icons"},Category {_catCategory = "Web Application Icons"}]}

fa_caret_square_o_right :: Icon
fa_caret_square_o_right = Icon {_icoName = IconName {_icnName = "Caret Square Outlined Right"}, _icoId = IconId {_iidId = "caret-square-o-right"}, _icoChar = Unicode {_uniChar = '\61778'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,2], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "forward"},Filter {_filtFilter = "next"}]), _icoCategories = fromList [Category {_catCategory = "Directional Icons"},Category {_catCategory = "Web Application Icons"}]}

fa_eur :: Icon
fa_eur = Icon {_icoName = IconName {_icnName = "Euro (EUR)"}, _icoId = IconId {_iidId = "eur"}, _icoChar = Unicode {_uniChar = '\61779'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,2], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Currency Icons"}]}

fa_gbp :: Icon
fa_gbp = Icon {_icoName = IconName {_icnName = "GBP"}, _icoId = IconId {_iidId = "gbp"}, _icoChar = Unicode {_uniChar = '\61780'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,2], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Currency Icons"}]}

fa_usd :: Icon
fa_usd = Icon {_icoName = IconName {_icnName = "US Dollar"}, _icoId = IconId {_iidId = "usd"}, _icoChar = Unicode {_uniChar = '\61781'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,2], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Currency Icons"}]}

fa_inr :: Icon
fa_inr = Icon {_icoName = IconName {_icnName = "Indian Rupee (INR)"}, _icoId = IconId {_iidId = "inr"}, _icoChar = Unicode {_uniChar = '\61782'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,2], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Currency Icons"}]}

fa_jpy :: Icon
fa_jpy = Icon {_icoName = IconName {_icnName = "Japanese Yen (JPY)"}, _icoId = IconId {_iidId = "jpy"}, _icoChar = Unicode {_uniChar = '\61783'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,2], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Currency Icons"}]}

fa_rub :: Icon
fa_rub = Icon {_icoName = IconName {_icnName = "Russian Ruble (RUB)"}, _icoId = IconId {_iidId = "rub"}, _icoChar = Unicode {_uniChar = '\61784'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,0], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Currency Icons"}]}

fa_krw :: Icon
fa_krw = Icon {_icoName = IconName {_icnName = "Korean Won (KRW)"}, _icoId = IconId {_iidId = "krw"}, _icoChar = Unicode {_uniChar = '\61785'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,2], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Currency Icons"}]}

fa_btc :: Icon
fa_btc = Icon {_icoName = IconName {_icnName = "Bitcoin (BTC)"}, _icoId = IconId {_iidId = "btc"}, _icoChar = Unicode {_uniChar = '\61786'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,2], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Brand Icons"},Category {_catCategory = "Currency Icons"}]}

fa_file :: Icon
fa_file = Icon {_icoName = IconName {_icnName = "File"}, _icoId = IconId {_iidId = "file"}, _icoChar = Unicode {_uniChar = '\61787'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,2], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "document"},Filter {_filtFilter = "new"},Filter {_filtFilter = "page"},Filter {_filtFilter = "pdf"}]), _icoCategories = fromList [Category {_catCategory = "File Type Icons"},Category {_catCategory = "Text Editor Icons"}]}

fa_file_text :: Icon
fa_file_text = Icon {_icoName = IconName {_icnName = "File Text"}, _icoId = IconId {_iidId = "file-text"}, _icoChar = Unicode {_uniChar = '\61788'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,2], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "document"},Filter {_filtFilter = "new"},Filter {_filtFilter = "page"},Filter {_filtFilter = "pdf"}]), _icoCategories = fromList [Category {_catCategory = "File Type Icons"},Category {_catCategory = "Text Editor Icons"}]}

fa_sort_alpha_asc :: Icon
fa_sort_alpha_asc = Icon {_icoName = IconName {_icnName = "Sort Alpha Ascending"}, _icoId = IconId {_iidId = "sort-alpha-asc"}, _icoChar = Unicode {_uniChar = '\61789'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,2], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_sort_alpha_desc :: Icon
fa_sort_alpha_desc = Icon {_icoName = IconName {_icnName = "Sort Alpha Descending"}, _icoId = IconId {_iidId = "sort-alpha-desc"}, _icoChar = Unicode {_uniChar = '\61790'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,2], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_sort_amount_asc :: Icon
fa_sort_amount_asc = Icon {_icoName = IconName {_icnName = "Sort Amount Ascending"}, _icoId = IconId {_iidId = "sort-amount-asc"}, _icoChar = Unicode {_uniChar = '\61792'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,2], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_sort_amount_desc :: Icon
fa_sort_amount_desc = Icon {_icoName = IconName {_icnName = "Sort Amount Descending"}, _icoId = IconId {_iidId = "sort-amount-desc"}, _icoChar = Unicode {_uniChar = '\61793'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,2], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_sort_numeric_asc :: Icon
fa_sort_numeric_asc = Icon {_icoName = IconName {_icnName = "Sort Numeric Ascending"}, _icoId = IconId {_iidId = "sort-numeric-asc"}, _icoChar = Unicode {_uniChar = '\61794'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,2], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "numbers"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_sort_numeric_desc :: Icon
fa_sort_numeric_desc = Icon {_icoName = IconName {_icnName = "Sort Numeric Descending"}, _icoId = IconId {_iidId = "sort-numeric-desc"}, _icoChar = Unicode {_uniChar = '\61795'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,2], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "numbers"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_thumbs_up :: Icon
fa_thumbs_up = Icon {_icoName = IconName {_icnName = "thumbs-up"}, _icoId = IconId {_iidId = "thumbs-up"}, _icoChar = Unicode {_uniChar = '\61796'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,2], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "agree"},Filter {_filtFilter = "approve"},Filter {_filtFilter = "favorite"},Filter {_filtFilter = "hand"},Filter {_filtFilter = "like"}]), _icoCategories = fromList [Category {_catCategory = "Hand Icons"},Category {_catCategory = "Web Application Icons"}]}

fa_thumbs_down :: Icon
fa_thumbs_down = Icon {_icoName = IconName {_icnName = "thumbs-down"}, _icoId = IconId {_iidId = "thumbs-down"}, _icoChar = Unicode {_uniChar = '\61797'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,2], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "disagree"},Filter {_filtFilter = "disapprove"},Filter {_filtFilter = "dislike"},Filter {_filtFilter = "hand"}]), _icoCategories = fromList [Category {_catCategory = "Hand Icons"},Category {_catCategory = "Web Application Icons"}]}

fa_youtube_square :: Icon
fa_youtube_square = Icon {_icoName = IconName {_icnName = "YouTube Square"}, _icoId = IconId {_iidId = "youtube-square"}, _icoChar = Unicode {_uniChar = '\61798'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,2], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "film"},Filter {_filtFilter = "video"}]), _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_youtube :: Icon
fa_youtube = Icon {_icoName = IconName {_icnName = "YouTube"}, _icoId = IconId {_iidId = "youtube"}, _icoChar = Unicode {_uniChar = '\61799'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,2], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "film"},Filter {_filtFilter = "video"}]), _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_xing :: Icon
fa_xing = Icon {_icoName = IconName {_icnName = "Xing"}, _icoId = IconId {_iidId = "xing"}, _icoChar = Unicode {_uniChar = '\61800'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,2], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_xing_square :: Icon
fa_xing_square = Icon {_icoName = IconName {_icnName = "Xing Square"}, _icoId = IconId {_iidId = "xing-square"}, _icoChar = Unicode {_uniChar = '\61801'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,2], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_youtube_play :: Icon
fa_youtube_play = Icon {_icoName = IconName {_icnName = "YouTube Play"}, _icoId = IconId {_iidId = "youtube-play"}, _icoChar = Unicode {_uniChar = '\61802'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,2], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "playing"},Filter {_filtFilter = "start"}]), _icoCategories = fromList [Category {_catCategory = "Brand Icons"},Category {_catCategory = "Video Player Icons"}]}

fa_dropbox :: Icon
fa_dropbox = Icon {_icoName = IconName {_icnName = "Dropbox"}, _icoId = IconId {_iidId = "dropbox"}, _icoChar = Unicode {_uniChar = '\61803'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,2], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_stack_overflow :: Icon
fa_stack_overflow = Icon {_icoName = IconName {_icnName = "Stack Overflow"}, _icoId = IconId {_iidId = "stack-overflow"}, _icoChar = Unicode {_uniChar = '\61804'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,2], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_instagram :: Icon
fa_instagram = Icon {_icoName = IconName {_icnName = "Instagram"}, _icoId = IconId {_iidId = "instagram"}, _icoChar = Unicode {_uniChar = '\61805'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,2], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_flickr :: Icon
fa_flickr = Icon {_icoName = IconName {_icnName = "Flickr"}, _icoId = IconId {_iidId = "flickr"}, _icoChar = Unicode {_uniChar = '\61806'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,2], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_adn :: Icon
fa_adn = Icon {_icoName = IconName {_icnName = "App.net"}, _icoId = IconId {_iidId = "adn"}, _icoChar = Unicode {_uniChar = '\61808'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,2], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_bitbucket :: Icon
fa_bitbucket = Icon {_icoName = IconName {_icnName = "Bitbucket"}, _icoId = IconId {_iidId = "bitbucket"}, _icoChar = Unicode {_uniChar = '\61809'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,2], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "git"}]), _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_bitbucket_square :: Icon
fa_bitbucket_square = Icon {_icoName = IconName {_icnName = "Bitbucket Square"}, _icoId = IconId {_iidId = "bitbucket-square"}, _icoChar = Unicode {_uniChar = '\61810'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,2], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "git"}]), _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_tumblr :: Icon
fa_tumblr = Icon {_icoName = IconName {_icnName = "Tumblr"}, _icoId = IconId {_iidId = "tumblr"}, _icoChar = Unicode {_uniChar = '\61811'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,2], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_tumblr_square :: Icon
fa_tumblr_square = Icon {_icoName = IconName {_icnName = "Tumblr Square"}, _icoId = IconId {_iidId = "tumblr-square"}, _icoChar = Unicode {_uniChar = '\61812'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,2], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_long_arrow_down :: Icon
fa_long_arrow_down = Icon {_icoName = IconName {_icnName = "Long Arrow Down"}, _icoId = IconId {_iidId = "long-arrow-down"}, _icoChar = Unicode {_uniChar = '\61813'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,2], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Directional Icons"}]}

fa_long_arrow_up :: Icon
fa_long_arrow_up = Icon {_icoName = IconName {_icnName = "Long Arrow Up"}, _icoId = IconId {_iidId = "long-arrow-up"}, _icoChar = Unicode {_uniChar = '\61814'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,2], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Directional Icons"}]}

fa_long_arrow_left :: Icon
fa_long_arrow_left = Icon {_icoName = IconName {_icnName = "Long Arrow Left"}, _icoId = IconId {_iidId = "long-arrow-left"}, _icoChar = Unicode {_uniChar = '\61815'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,2], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "back"},Filter {_filtFilter = "previous"}]), _icoCategories = fromList [Category {_catCategory = "Directional Icons"}]}

fa_long_arrow_right :: Icon
fa_long_arrow_right = Icon {_icoName = IconName {_icnName = "Long Arrow Right"}, _icoId = IconId {_iidId = "long-arrow-right"}, _icoChar = Unicode {_uniChar = '\61816'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,2], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Directional Icons"}]}

fa_apple :: Icon
fa_apple = Icon {_icoName = IconName {_icnName = "Apple"}, _icoId = IconId {_iidId = "apple"}, _icoChar = Unicode {_uniChar = '\61817'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,2], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "food"},Filter {_filtFilter = "osx"}]), _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_windows :: Icon
fa_windows = Icon {_icoName = IconName {_icnName = "Windows"}, _icoId = IconId {_iidId = "windows"}, _icoChar = Unicode {_uniChar = '\61818'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,2], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "microsoft"}]), _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_android :: Icon
fa_android = Icon {_icoName = IconName {_icnName = "Android"}, _icoId = IconId {_iidId = "android"}, _icoChar = Unicode {_uniChar = '\61819'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,2], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "robot"}]), _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_linux :: Icon
fa_linux = Icon {_icoName = IconName {_icnName = "Linux"}, _icoId = IconId {_iidId = "linux"}, _icoChar = Unicode {_uniChar = '\61820'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,2], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "tux"}]), _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_dribbble :: Icon
fa_dribbble = Icon {_icoName = IconName {_icnName = "Dribbble"}, _icoId = IconId {_iidId = "dribbble"}, _icoChar = Unicode {_uniChar = '\61821'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,2], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_skype :: Icon
fa_skype = Icon {_icoName = IconName {_icnName = "Skype"}, _icoId = IconId {_iidId = "skype"}, _icoChar = Unicode {_uniChar = '\61822'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,2], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_foursquare :: Icon
fa_foursquare = Icon {_icoName = IconName {_icnName = "Foursquare"}, _icoId = IconId {_iidId = "foursquare"}, _icoChar = Unicode {_uniChar = '\61824'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,2], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_trello :: Icon
fa_trello = Icon {_icoName = IconName {_icnName = "Trello"}, _icoId = IconId {_iidId = "trello"}, _icoChar = Unicode {_uniChar = '\61825'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,2], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_female :: Icon
fa_female = Icon {_icoName = IconName {_icnName = "Female"}, _icoId = IconId {_iidId = "female"}, _icoChar = Unicode {_uniChar = '\61826'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,2], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "person"},Filter {_filtFilter = "profile"},Filter {_filtFilter = "user"},Filter {_filtFilter = "woman"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_male :: Icon
fa_male = Icon {_icoName = IconName {_icnName = "Male"}, _icoId = IconId {_iidId = "male"}, _icoChar = Unicode {_uniChar = '\61827'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,2], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "man"},Filter {_filtFilter = "person"},Filter {_filtFilter = "profile"},Filter {_filtFilter = "user"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_gratipay :: Icon
fa_gratipay = Icon {_icoName = IconName {_icnName = "Gratipay (Gittip)"}, _icoId = IconId {_iidId = "gratipay"}, _icoChar = Unicode {_uniChar = '\61828'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,2], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "favorite"},Filter {_filtFilter = "heart"},Filter {_filtFilter = "like"},Filter {_filtFilter = "love"}]), _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_sun_o :: Icon
fa_sun_o = Icon {_icoName = IconName {_icnName = "Sun Outlined"}, _icoId = IconId {_iidId = "sun-o"}, _icoChar = Unicode {_uniChar = '\61829'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,2], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "brighten"},Filter {_filtFilter = "contrast"},Filter {_filtFilter = "day"},Filter {_filtFilter = "lighter"},Filter {_filtFilter = "weather"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_moon_o :: Icon
fa_moon_o = Icon {_icoName = IconName {_icnName = "Moon Outlined"}, _icoId = IconId {_iidId = "moon-o"}, _icoChar = Unicode {_uniChar = '\61830'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,2], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "contrast"},Filter {_filtFilter = "darker"},Filter {_filtFilter = "night"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_archive :: Icon
fa_archive = Icon {_icoName = IconName {_icnName = "Archive"}, _icoId = IconId {_iidId = "archive"}, _icoChar = Unicode {_uniChar = '\61831'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,2], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "box"},Filter {_filtFilter = "storage"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_bug :: Icon
fa_bug = Icon {_icoName = IconName {_icnName = "Bug"}, _icoId = IconId {_iidId = "bug"}, _icoChar = Unicode {_uniChar = '\61832'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,2], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "insect"},Filter {_filtFilter = "report"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_vk :: Icon
fa_vk = Icon {_icoName = IconName {_icnName = "VK"}, _icoId = IconId {_iidId = "vk"}, _icoChar = Unicode {_uniChar = '\61833'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,2], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_weibo :: Icon
fa_weibo = Icon {_icoName = IconName {_icnName = "Weibo"}, _icoId = IconId {_iidId = "weibo"}, _icoChar = Unicode {_uniChar = '\61834'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,2], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_renren :: Icon
fa_renren = Icon {_icoName = IconName {_icnName = "Renren"}, _icoId = IconId {_iidId = "renren"}, _icoChar = Unicode {_uniChar = '\61835'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [3,2], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_pagelines :: Icon
fa_pagelines = Icon {_icoName = IconName {_icnName = "Pagelines"}, _icoId = IconId {_iidId = "pagelines"}, _icoChar = Unicode {_uniChar = '\61836'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "eco"},Filter {_filtFilter = "leaf"},Filter {_filtFilter = "leaves"},Filter {_filtFilter = "nature"},Filter {_filtFilter = "plant"},Filter {_filtFilter = "tree"}]), _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_stack_exchange :: Icon
fa_stack_exchange = Icon {_icoName = IconName {_icnName = "Stack Exchange"}, _icoId = IconId {_iidId = "stack-exchange"}, _icoChar = Unicode {_uniChar = '\61837'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,0], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_arrow_circle_o_right :: Icon
fa_arrow_circle_o_right = Icon {_icoName = IconName {_icnName = "Arrow Circle Outlined Right"}, _icoId = IconId {_iidId = "arrow-circle-o-right"}, _icoChar = Unicode {_uniChar = '\61838'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "forward"},Filter {_filtFilter = "next"}]), _icoCategories = fromList [Category {_catCategory = "Directional Icons"}]}

fa_arrow_circle_o_left :: Icon
fa_arrow_circle_o_left = Icon {_icoName = IconName {_icnName = "Arrow Circle Outlined Left"}, _icoId = IconId {_iidId = "arrow-circle-o-left"}, _icoChar = Unicode {_uniChar = '\61840'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "back"},Filter {_filtFilter = "previous"}]), _icoCategories = fromList [Category {_catCategory = "Directional Icons"}]}

fa_caret_square_o_left :: Icon
fa_caret_square_o_left = Icon {_icoName = IconName {_icnName = "Caret Square Outlined Left"}, _icoId = IconId {_iidId = "caret-square-o-left"}, _icoChar = Unicode {_uniChar = '\61841'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "back"},Filter {_filtFilter = "previous"}]), _icoCategories = fromList [Category {_catCategory = "Directional Icons"},Category {_catCategory = "Web Application Icons"}]}

fa_dot_circle_o :: Icon
fa_dot_circle_o = Icon {_icoName = IconName {_icnName = "Dot Circle Outlined"}, _icoId = IconId {_iidId = "dot-circle-o"}, _icoChar = Unicode {_uniChar = '\61842'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "bullseye"},Filter {_filtFilter = "notification"},Filter {_filtFilter = "target"}]), _icoCategories = fromList [Category {_catCategory = "Form Control Icons"},Category {_catCategory = "Web Application Icons"}]}

fa_wheelchair :: Icon
fa_wheelchair = Icon {_icoName = IconName {_icnName = "Wheelchair"}, _icoId = IconId {_iidId = "wheelchair"}, _icoChar = Unicode {_uniChar = '\61843'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "accessibile"},Filter {_filtFilter = "accessibility"},Filter {_filtFilter = "handicap"},Filter {_filtFilter = "person"}]), _icoCategories = fromList [Category {_catCategory = "Accessibility Icons"},Category {_catCategory = "Medical Icons"},Category {_catCategory = "Transportation Icons"},Category {_catCategory = "Web Application Icons"}]}

fa_vimeo_square :: Icon
fa_vimeo_square = Icon {_icoName = IconName {_icnName = "Vimeo Square"}, _icoId = IconId {_iidId = "vimeo-square"}, _icoChar = Unicode {_uniChar = '\61844'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,0], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_try :: Icon
fa_try = Icon {_icoName = IconName {_icnName = "Turkish Lira (TRY)"}, _icoId = IconId {_iidId = "try"}, _icoChar = Unicode {_uniChar = '\61845'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,0], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Currency Icons"}]}

fa_plus_square_o :: Icon
fa_plus_square_o = Icon {_icoName = IconName {_icnName = "Plus Square Outlined"}, _icoId = IconId {_iidId = "plus-square-o"}, _icoChar = Unicode {_uniChar = '\61846'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,0], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "add"},Filter {_filtFilter = "create"},Filter {_filtFilter = "expand"},Filter {_filtFilter = "new"}]), _icoCategories = fromList [Category {_catCategory = "Form Control Icons"},Category {_catCategory = "Web Application Icons"}]}

fa_space_shuttle :: Icon
fa_space_shuttle = Icon {_icoName = IconName {_icnName = "Space Shuttle"}, _icoId = IconId {_iidId = "space-shuttle"}, _icoChar = Unicode {_uniChar = '\61847'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,1], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Transportation Icons"},Category {_catCategory = "Web Application Icons"}]}

fa_slack :: Icon
fa_slack = Icon {_icoName = IconName {_icnName = "Slack Logo"}, _icoId = IconId {_iidId = "slack"}, _icoChar = Unicode {_uniChar = '\61848'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,1], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "anchor"},Filter {_filtFilter = "hash"},Filter {_filtFilter = "hashtag"}]), _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_envelope_square :: Icon
fa_envelope_square = Icon {_icoName = IconName {_icnName = "Envelope Square"}, _icoId = IconId {_iidId = "envelope-square"}, _icoChar = Unicode {_uniChar = '\61849'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,1], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_wordpress :: Icon
fa_wordpress = Icon {_icoName = IconName {_icnName = "WordPress Logo"}, _icoId = IconId {_iidId = "wordpress"}, _icoChar = Unicode {_uniChar = '\61850'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,1], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_openid :: Icon
fa_openid = Icon {_icoName = IconName {_icnName = "OpenID"}, _icoId = IconId {_iidId = "openid"}, _icoChar = Unicode {_uniChar = '\61851'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,1], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_university :: Icon
fa_university = Icon {_icoName = IconName {_icnName = "University"}, _icoId = IconId {_iidId = "university"}, _icoChar = Unicode {_uniChar = '\61852'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,1], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_graduation_cap :: Icon
fa_graduation_cap = Icon {_icoName = IconName {_icnName = "Graduation Cap"}, _icoId = IconId {_iidId = "graduation-cap"}, _icoChar = Unicode {_uniChar = '\61853'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,1], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "learning"},Filter {_filtFilter = "school"},Filter {_filtFilter = "student"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_yahoo :: Icon
fa_yahoo = Icon {_icoName = IconName {_icnName = "Yahoo Logo"}, _icoId = IconId {_iidId = "yahoo"}, _icoChar = Unicode {_uniChar = '\61854'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,1], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_google :: Icon
fa_google = Icon {_icoName = IconName {_icnName = "Google Logo"}, _icoId = IconId {_iidId = "google"}, _icoChar = Unicode {_uniChar = '\61856'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,1], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_reddit :: Icon
fa_reddit = Icon {_icoName = IconName {_icnName = "reddit Logo"}, _icoId = IconId {_iidId = "reddit"}, _icoChar = Unicode {_uniChar = '\61857'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,1], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_reddit_square :: Icon
fa_reddit_square = Icon {_icoName = IconName {_icnName = "reddit Square"}, _icoId = IconId {_iidId = "reddit-square"}, _icoChar = Unicode {_uniChar = '\61858'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,1], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_stumbleupon_circle :: Icon
fa_stumbleupon_circle = Icon {_icoName = IconName {_icnName = "StumbleUpon Circle"}, _icoId = IconId {_iidId = "stumbleupon-circle"}, _icoChar = Unicode {_uniChar = '\61859'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,1], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_stumbleupon :: Icon
fa_stumbleupon = Icon {_icoName = IconName {_icnName = "StumbleUpon Logo"}, _icoId = IconId {_iidId = "stumbleupon"}, _icoChar = Unicode {_uniChar = '\61860'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,1], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_delicious :: Icon
fa_delicious = Icon {_icoName = IconName {_icnName = "Delicious Logo"}, _icoId = IconId {_iidId = "delicious"}, _icoChar = Unicode {_uniChar = '\61861'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,1], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_digg :: Icon
fa_digg = Icon {_icoName = IconName {_icnName = "Digg Logo"}, _icoId = IconId {_iidId = "digg"}, _icoChar = Unicode {_uniChar = '\61862'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,1], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_pied_piper :: Icon
fa_pied_piper = Icon {_icoName = IconName {_icnName = "Pied Piper Logo"}, _icoId = IconId {_iidId = "pied-piper"}, _icoChar = Unicode {_uniChar = '\61863'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,1], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_pied_piper_alt :: Icon
fa_pied_piper_alt = Icon {_icoName = IconName {_icnName = "Pied Piper Alternate Logo"}, _icoId = IconId {_iidId = "pied-piper-alt"}, _icoChar = Unicode {_uniChar = '\61864'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,1], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_drupal :: Icon
fa_drupal = Icon {_icoName = IconName {_icnName = "Drupal Logo"}, _icoId = IconId {_iidId = "drupal"}, _icoChar = Unicode {_uniChar = '\61865'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,1], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_joomla :: Icon
fa_joomla = Icon {_icoName = IconName {_icnName = "Joomla Logo"}, _icoId = IconId {_iidId = "joomla"}, _icoChar = Unicode {_uniChar = '\61866'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,1], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_language :: Icon
fa_language = Icon {_icoName = IconName {_icnName = "Language"}, _icoId = IconId {_iidId = "language"}, _icoChar = Unicode {_uniChar = '\61867'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,1], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_fax :: Icon
fa_fax = Icon {_icoName = IconName {_icnName = "Fax"}, _icoId = IconId {_iidId = "fax"}, _icoChar = Unicode {_uniChar = '\61868'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,1], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_building :: Icon
fa_building = Icon {_icoName = IconName {_icnName = "Building"}, _icoId = IconId {_iidId = "building"}, _icoChar = Unicode {_uniChar = '\61869'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,1], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "apartment"},Filter {_filtFilter = "business"},Filter {_filtFilter = "company"},Filter {_filtFilter = "office"},Filter {_filtFilter = "work"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_child :: Icon
fa_child = Icon {_icoName = IconName {_icnName = "Child"}, _icoId = IconId {_iidId = "child"}, _icoChar = Unicode {_uniChar = '\61870'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,1], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_paw :: Icon
fa_paw = Icon {_icoName = IconName {_icnName = "Paw"}, _icoId = IconId {_iidId = "paw"}, _icoChar = Unicode {_uniChar = '\61872'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,1], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "pet"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_spoon :: Icon
fa_spoon = Icon {_icoName = IconName {_icnName = "spoon"}, _icoId = IconId {_iidId = "spoon"}, _icoChar = Unicode {_uniChar = '\61873'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,1], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_cube :: Icon
fa_cube = Icon {_icoName = IconName {_icnName = "Cube"}, _icoId = IconId {_iidId = "cube"}, _icoChar = Unicode {_uniChar = '\61874'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,1], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_cubes :: Icon
fa_cubes = Icon {_icoName = IconName {_icnName = "Cubes"}, _icoId = IconId {_iidId = "cubes"}, _icoChar = Unicode {_uniChar = '\61875'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,1], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_behance :: Icon
fa_behance = Icon {_icoName = IconName {_icnName = "Behance"}, _icoId = IconId {_iidId = "behance"}, _icoChar = Unicode {_uniChar = '\61876'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,1], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_behance_square :: Icon
fa_behance_square = Icon {_icoName = IconName {_icnName = "Behance Square"}, _icoId = IconId {_iidId = "behance-square"}, _icoChar = Unicode {_uniChar = '\61877'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,1], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_steam :: Icon
fa_steam = Icon {_icoName = IconName {_icnName = "Steam"}, _icoId = IconId {_iidId = "steam"}, _icoChar = Unicode {_uniChar = '\61878'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,1], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_steam_square :: Icon
fa_steam_square = Icon {_icoName = IconName {_icnName = "Steam Square"}, _icoId = IconId {_iidId = "steam-square"}, _icoChar = Unicode {_uniChar = '\61879'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,1], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_recycle :: Icon
fa_recycle = Icon {_icoName = IconName {_icnName = "Recycle"}, _icoId = IconId {_iidId = "recycle"}, _icoChar = Unicode {_uniChar = '\61880'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,1], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_car :: Icon
fa_car = Icon {_icoName = IconName {_icnName = "Car"}, _icoId = IconId {_iidId = "car"}, _icoChar = Unicode {_uniChar = '\61881'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,1], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "vehicle"}]), _icoCategories = fromList [Category {_catCategory = "Transportation Icons"},Category {_catCategory = "Web Application Icons"}]}

fa_taxi :: Icon
fa_taxi = Icon {_icoName = IconName {_icnName = "Taxi"}, _icoId = IconId {_iidId = "taxi"}, _icoChar = Unicode {_uniChar = '\61882'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,1], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "vehicle"}]), _icoCategories = fromList [Category {_catCategory = "Transportation Icons"},Category {_catCategory = "Web Application Icons"}]}

fa_tree :: Icon
fa_tree = Icon {_icoName = IconName {_icnName = "Tree"}, _icoId = IconId {_iidId = "tree"}, _icoChar = Unicode {_uniChar = '\61883'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,1], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_spotify :: Icon
fa_spotify = Icon {_icoName = IconName {_icnName = "Spotify"}, _icoId = IconId {_iidId = "spotify"}, _icoChar = Unicode {_uniChar = '\61884'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,1], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_deviantart :: Icon
fa_deviantart = Icon {_icoName = IconName {_icnName = "deviantART"}, _icoId = IconId {_iidId = "deviantart"}, _icoChar = Unicode {_uniChar = '\61885'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,1], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_soundcloud :: Icon
fa_soundcloud = Icon {_icoName = IconName {_icnName = "SoundCloud"}, _icoId = IconId {_iidId = "soundcloud"}, _icoChar = Unicode {_uniChar = '\61886'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,1], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_database :: Icon
fa_database = Icon {_icoName = IconName {_icnName = "Database"}, _icoId = IconId {_iidId = "database"}, _icoChar = Unicode {_uniChar = '\61888'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,1], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_file_pdf_o :: Icon
fa_file_pdf_o = Icon {_icoName = IconName {_icnName = "PDF File Outlined"}, _icoId = IconId {_iidId = "file-pdf-o"}, _icoChar = Unicode {_uniChar = '\61889'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,1], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "File Type Icons"},Category {_catCategory = "Web Application Icons"}]}

fa_file_word_o :: Icon
fa_file_word_o = Icon {_icoName = IconName {_icnName = "Word File Outlined"}, _icoId = IconId {_iidId = "file-word-o"}, _icoChar = Unicode {_uniChar = '\61890'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,1], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "File Type Icons"},Category {_catCategory = "Web Application Icons"}]}

fa_file_excel_o :: Icon
fa_file_excel_o = Icon {_icoName = IconName {_icnName = "Excel File Outlined"}, _icoId = IconId {_iidId = "file-excel-o"}, _icoChar = Unicode {_uniChar = '\61891'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,1], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "File Type Icons"},Category {_catCategory = "Web Application Icons"}]}

fa_file_powerpoint_o :: Icon
fa_file_powerpoint_o = Icon {_icoName = IconName {_icnName = "Powerpoint File Outlined"}, _icoId = IconId {_iidId = "file-powerpoint-o"}, _icoChar = Unicode {_uniChar = '\61892'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,1], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "File Type Icons"},Category {_catCategory = "Web Application Icons"}]}

fa_file_image_o :: Icon
fa_file_image_o = Icon {_icoName = IconName {_icnName = "Image File Outlined"}, _icoId = IconId {_iidId = "file-image-o"}, _icoChar = Unicode {_uniChar = '\61893'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,1], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "File Type Icons"},Category {_catCategory = "Web Application Icons"}]}

fa_file_archive_o :: Icon
fa_file_archive_o = Icon {_icoName = IconName {_icnName = "Archive File Outlined"}, _icoId = IconId {_iidId = "file-archive-o"}, _icoChar = Unicode {_uniChar = '\61894'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,1], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "File Type Icons"},Category {_catCategory = "Web Application Icons"}]}

fa_file_audio_o :: Icon
fa_file_audio_o = Icon {_icoName = IconName {_icnName = "Audio File Outlined"}, _icoId = IconId {_iidId = "file-audio-o"}, _icoChar = Unicode {_uniChar = '\61895'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,1], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "File Type Icons"},Category {_catCategory = "Web Application Icons"}]}

fa_file_video_o :: Icon
fa_file_video_o = Icon {_icoName = IconName {_icnName = "Video File Outlined"}, _icoId = IconId {_iidId = "file-video-o"}, _icoChar = Unicode {_uniChar = '\61896'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,1], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "File Type Icons"},Category {_catCategory = "Web Application Icons"}]}

fa_file_code_o :: Icon
fa_file_code_o = Icon {_icoName = IconName {_icnName = "Code File Outlined"}, _icoId = IconId {_iidId = "file-code-o"}, _icoChar = Unicode {_uniChar = '\61897'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,1], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "File Type Icons"},Category {_catCategory = "Web Application Icons"}]}

fa_vine :: Icon
fa_vine = Icon {_icoName = IconName {_icnName = "Vine"}, _icoId = IconId {_iidId = "vine"}, _icoChar = Unicode {_uniChar = '\61898'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,1], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_codepen :: Icon
fa_codepen = Icon {_icoName = IconName {_icnName = "Codepen"}, _icoId = IconId {_iidId = "codepen"}, _icoChar = Unicode {_uniChar = '\61899'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,1], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_jsfiddle :: Icon
fa_jsfiddle = Icon {_icoName = IconName {_icnName = "jsFiddle"}, _icoId = IconId {_iidId = "jsfiddle"}, _icoChar = Unicode {_uniChar = '\61900'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,1], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_life_ring :: Icon
fa_life_ring = Icon {_icoName = IconName {_icnName = "Life Ring"}, _icoId = IconId {_iidId = "life-ring"}, _icoChar = Unicode {_uniChar = '\61901'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,1], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_circle_o_notch :: Icon
fa_circle_o_notch = Icon {_icoName = IconName {_icnName = "Circle Outlined Notched"}, _icoId = IconId {_iidId = "circle-o-notch"}, _icoChar = Unicode {_uniChar = '\61902'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,1], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Spinner Icons"},Category {_catCategory = "Web Application Icons"}]}

fa_rebel :: Icon
fa_rebel = Icon {_icoName = IconName {_icnName = "Rebel Alliance"}, _icoId = IconId {_iidId = "rebel"}, _icoChar = Unicode {_uniChar = '\61904'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,1], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_empire :: Icon
fa_empire = Icon {_icoName = IconName {_icnName = "Galactic Empire"}, _icoId = IconId {_iidId = "empire"}, _icoChar = Unicode {_uniChar = '\61905'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,1], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_git_square :: Icon
fa_git_square = Icon {_icoName = IconName {_icnName = "Git Square"}, _icoId = IconId {_iidId = "git-square"}, _icoChar = Unicode {_uniChar = '\61906'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,1], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_git :: Icon
fa_git = Icon {_icoName = IconName {_icnName = "Git"}, _icoId = IconId {_iidId = "git"}, _icoChar = Unicode {_uniChar = '\61907'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,1], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_hacker_news :: Icon
fa_hacker_news = Icon {_icoName = IconName {_icnName = "Hacker News"}, _icoId = IconId {_iidId = "hacker-news"}, _icoChar = Unicode {_uniChar = '\61908'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,1], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_tencent_weibo :: Icon
fa_tencent_weibo = Icon {_icoName = IconName {_icnName = "Tencent Weibo"}, _icoId = IconId {_iidId = "tencent-weibo"}, _icoChar = Unicode {_uniChar = '\61909'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,1], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_qq :: Icon
fa_qq = Icon {_icoName = IconName {_icnName = "QQ"}, _icoId = IconId {_iidId = "qq"}, _icoChar = Unicode {_uniChar = '\61910'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,1], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_weixin :: Icon
fa_weixin = Icon {_icoName = IconName {_icnName = "Weixin (WeChat)"}, _icoId = IconId {_iidId = "weixin"}, _icoChar = Unicode {_uniChar = '\61911'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,1], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_paper_plane :: Icon
fa_paper_plane = Icon {_icoName = IconName {_icnName = "Paper Plane"}, _icoId = IconId {_iidId = "paper-plane"}, _icoChar = Unicode {_uniChar = '\61912'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,1], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_paper_plane_o :: Icon
fa_paper_plane_o = Icon {_icoName = IconName {_icnName = "Paper Plane Outlined"}, _icoId = IconId {_iidId = "paper-plane-o"}, _icoChar = Unicode {_uniChar = '\61913'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,1], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_history :: Icon
fa_history = Icon {_icoName = IconName {_icnName = "History"}, _icoId = IconId {_iidId = "history"}, _icoChar = Unicode {_uniChar = '\61914'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,1], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_circle_thin :: Icon
fa_circle_thin = Icon {_icoName = IconName {_icnName = "Circle Outlined Thin"}, _icoId = IconId {_iidId = "circle-thin"}, _icoChar = Unicode {_uniChar = '\61915'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,1], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_header :: Icon
fa_header = Icon {_icoName = IconName {_icnName = "header"}, _icoId = IconId {_iidId = "header"}, _icoChar = Unicode {_uniChar = '\61916'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,1], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "heading"}]), _icoCategories = fromList [Category {_catCategory = "Text Editor Icons"}]}

fa_paragraph :: Icon
fa_paragraph = Icon {_icoName = IconName {_icnName = "paragraph"}, _icoId = IconId {_iidId = "paragraph"}, _icoChar = Unicode {_uniChar = '\61917'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,1], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Text Editor Icons"}]}

fa_sliders :: Icon
fa_sliders = Icon {_icoName = IconName {_icnName = "Sliders"}, _icoId = IconId {_iidId = "sliders"}, _icoChar = Unicode {_uniChar = '\61918'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,1], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "settings"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_share_alt :: Icon
fa_share_alt = Icon {_icoName = IconName {_icnName = "Share Alt"}, _icoId = IconId {_iidId = "share-alt"}, _icoChar = Unicode {_uniChar = '\61920'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,1], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Brand Icons"},Category {_catCategory = "Web Application Icons"}]}

fa_share_alt_square :: Icon
fa_share_alt_square = Icon {_icoName = IconName {_icnName = "Share Alt Square"}, _icoId = IconId {_iidId = "share-alt-square"}, _icoChar = Unicode {_uniChar = '\61921'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,1], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Brand Icons"},Category {_catCategory = "Web Application Icons"}]}

fa_bomb :: Icon
fa_bomb = Icon {_icoName = IconName {_icnName = "Bomb"}, _icoId = IconId {_iidId = "bomb"}, _icoChar = Unicode {_uniChar = '\61922'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,1], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_futbol_o :: Icon
fa_futbol_o = Icon {_icoName = IconName {_icnName = "Futbol Outlined"}, _icoId = IconId {_iidId = "futbol-o"}, _icoChar = Unicode {_uniChar = '\61923'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,2], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_tty :: Icon
fa_tty = Icon {_icoName = IconName {_icnName = "TTY"}, _icoId = IconId {_iidId = "tty"}, _icoChar = Unicode {_uniChar = '\61924'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,2], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Accessibility Icons"},Category {_catCategory = "Web Application Icons"}]}

fa_binoculars :: Icon
fa_binoculars = Icon {_icoName = IconName {_icnName = "Binoculars"}, _icoId = IconId {_iidId = "binoculars"}, _icoChar = Unicode {_uniChar = '\61925'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,2], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_plug :: Icon
fa_plug = Icon {_icoName = IconName {_icnName = "Plug"}, _icoId = IconId {_iidId = "plug"}, _icoChar = Unicode {_uniChar = '\61926'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,2], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "connect"},Filter {_filtFilter = "power"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_slideshare :: Icon
fa_slideshare = Icon {_icoName = IconName {_icnName = "Slideshare"}, _icoId = IconId {_iidId = "slideshare"}, _icoChar = Unicode {_uniChar = '\61927'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,2], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_twitch :: Icon
fa_twitch = Icon {_icoName = IconName {_icnName = "Twitch"}, _icoId = IconId {_iidId = "twitch"}, _icoChar = Unicode {_uniChar = '\61928'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,2], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_yelp :: Icon
fa_yelp = Icon {_icoName = IconName {_icnName = "Yelp"}, _icoId = IconId {_iidId = "yelp"}, _icoChar = Unicode {_uniChar = '\61929'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,2], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_newspaper_o :: Icon
fa_newspaper_o = Icon {_icoName = IconName {_icnName = "Newspaper Outlined"}, _icoId = IconId {_iidId = "newspaper-o"}, _icoChar = Unicode {_uniChar = '\61930'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,2], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "press"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_wifi :: Icon
fa_wifi = Icon {_icoName = IconName {_icnName = "WiFi"}, _icoId = IconId {_iidId = "wifi"}, _icoChar = Unicode {_uniChar = '\61931'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,2], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_calculator :: Icon
fa_calculator = Icon {_icoName = IconName {_icnName = "Calculator"}, _icoId = IconId {_iidId = "calculator"}, _icoChar = Unicode {_uniChar = '\61932'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,2], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_paypal :: Icon
fa_paypal = Icon {_icoName = IconName {_icnName = "Paypal"}, _icoId = IconId {_iidId = "paypal"}, _icoChar = Unicode {_uniChar = '\61933'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,2], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Brand Icons"},Category {_catCategory = "Payment Icons"}]}

fa_google_wallet :: Icon
fa_google_wallet = Icon {_icoName = IconName {_icnName = "Google Wallet"}, _icoId = IconId {_iidId = "google-wallet"}, _icoChar = Unicode {_uniChar = '\61934'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,2], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Brand Icons"},Category {_catCategory = "Payment Icons"}]}

fa_cc_visa :: Icon
fa_cc_visa = Icon {_icoName = IconName {_icnName = "Visa Credit Card"}, _icoId = IconId {_iidId = "cc-visa"}, _icoChar = Unicode {_uniChar = '\61936'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,2], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Brand Icons"},Category {_catCategory = "Payment Icons"}]}

fa_cc_mastercard :: Icon
fa_cc_mastercard = Icon {_icoName = IconName {_icnName = "MasterCard Credit Card"}, _icoId = IconId {_iidId = "cc-mastercard"}, _icoChar = Unicode {_uniChar = '\61937'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,2], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Brand Icons"},Category {_catCategory = "Payment Icons"}]}

fa_cc_discover :: Icon
fa_cc_discover = Icon {_icoName = IconName {_icnName = "Discover Credit Card"}, _icoId = IconId {_iidId = "cc-discover"}, _icoChar = Unicode {_uniChar = '\61938'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,2], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Brand Icons"},Category {_catCategory = "Payment Icons"}]}

fa_cc_amex :: Icon
fa_cc_amex = Icon {_icoName = IconName {_icnName = "American Express Credit Card"}, _icoId = IconId {_iidId = "cc-amex"}, _icoChar = Unicode {_uniChar = '\61939'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,2], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "amex"}]), _icoCategories = fromList [Category {_catCategory = "Brand Icons"},Category {_catCategory = "Payment Icons"}]}

fa_cc_paypal :: Icon
fa_cc_paypal = Icon {_icoName = IconName {_icnName = "Paypal Credit Card"}, _icoId = IconId {_iidId = "cc-paypal"}, _icoChar = Unicode {_uniChar = '\61940'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,2], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Brand Icons"},Category {_catCategory = "Payment Icons"}]}

fa_cc_stripe :: Icon
fa_cc_stripe = Icon {_icoName = IconName {_icnName = "Stripe Credit Card"}, _icoId = IconId {_iidId = "cc-stripe"}, _icoChar = Unicode {_uniChar = '\61941'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,2], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Brand Icons"},Category {_catCategory = "Payment Icons"}]}

fa_bell_slash :: Icon
fa_bell_slash = Icon {_icoName = IconName {_icnName = "Bell Slash"}, _icoId = IconId {_iidId = "bell-slash"}, _icoChar = Unicode {_uniChar = '\61942'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,2], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_bell_slash_o :: Icon
fa_bell_slash_o = Icon {_icoName = IconName {_icnName = "Bell Slash Outlined"}, _icoId = IconId {_iidId = "bell-slash-o"}, _icoChar = Unicode {_uniChar = '\61943'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,2], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_trash :: Icon
fa_trash = Icon {_icoName = IconName {_icnName = "Trash"}, _icoId = IconId {_iidId = "trash"}, _icoChar = Unicode {_uniChar = '\61944'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,2], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "delete"},Filter {_filtFilter = "garbage"},Filter {_filtFilter = "hide"},Filter {_filtFilter = "remove"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_copyright :: Icon
fa_copyright = Icon {_icoName = IconName {_icnName = "Copyright"}, _icoId = IconId {_iidId = "copyright"}, _icoChar = Unicode {_uniChar = '\61945'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,2], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_at :: Icon
fa_at = Icon {_icoName = IconName {_icnName = "At"}, _icoId = IconId {_iidId = "at"}, _icoChar = Unicode {_uniChar = '\61946'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,2], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_eyedropper :: Icon
fa_eyedropper = Icon {_icoName = IconName {_icnName = "Eyedropper"}, _icoId = IconId {_iidId = "eyedropper"}, _icoChar = Unicode {_uniChar = '\61947'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,2], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_paint_brush :: Icon
fa_paint_brush = Icon {_icoName = IconName {_icnName = "Paint Brush"}, _icoId = IconId {_iidId = "paint-brush"}, _icoChar = Unicode {_uniChar = '\61948'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,2], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_birthday_cake :: Icon
fa_birthday_cake = Icon {_icoName = IconName {_icnName = "Birthday Cake"}, _icoId = IconId {_iidId = "birthday-cake"}, _icoChar = Unicode {_uniChar = '\61949'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,2], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_area_chart :: Icon
fa_area_chart = Icon {_icoName = IconName {_icnName = "Area Chart"}, _icoId = IconId {_iidId = "area-chart"}, _icoChar = Unicode {_uniChar = '\61950'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,2], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "analytics"},Filter {_filtFilter = "graph"}]), _icoCategories = fromList [Category {_catCategory = "Chart Icons"},Category {_catCategory = "Web Application Icons"}]}

fa_pie_chart :: Icon
fa_pie_chart = Icon {_icoName = IconName {_icnName = "Pie Chart"}, _icoId = IconId {_iidId = "pie-chart"}, _icoChar = Unicode {_uniChar = '\61952'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,2], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "analytics"},Filter {_filtFilter = "graph"}]), _icoCategories = fromList [Category {_catCategory = "Chart Icons"},Category {_catCategory = "Web Application Icons"}]}

fa_line_chart :: Icon
fa_line_chart = Icon {_icoName = IconName {_icnName = "Line Chart"}, _icoId = IconId {_iidId = "line-chart"}, _icoChar = Unicode {_uniChar = '\61953'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,2], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "analytics"},Filter {_filtFilter = "graph"}]), _icoCategories = fromList [Category {_catCategory = "Chart Icons"},Category {_catCategory = "Web Application Icons"}]}

fa_lastfm :: Icon
fa_lastfm = Icon {_icoName = IconName {_icnName = "last.fm"}, _icoId = IconId {_iidId = "lastfm"}, _icoChar = Unicode {_uniChar = '\61954'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,2], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_lastfm_square :: Icon
fa_lastfm_square = Icon {_icoName = IconName {_icnName = "last.fm Square"}, _icoId = IconId {_iidId = "lastfm-square"}, _icoChar = Unicode {_uniChar = '\61955'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,2], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_toggle_off :: Icon
fa_toggle_off = Icon {_icoName = IconName {_icnName = "Toggle Off"}, _icoId = IconId {_iidId = "toggle-off"}, _icoChar = Unicode {_uniChar = '\61956'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,2], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_toggle_on :: Icon
fa_toggle_on = Icon {_icoName = IconName {_icnName = "Toggle On"}, _icoId = IconId {_iidId = "toggle-on"}, _icoChar = Unicode {_uniChar = '\61957'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,2], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_bicycle :: Icon
fa_bicycle = Icon {_icoName = IconName {_icnName = "Bicycle"}, _icoId = IconId {_iidId = "bicycle"}, _icoChar = Unicode {_uniChar = '\61958'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,2], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "bike"},Filter {_filtFilter = "vehicle"}]), _icoCategories = fromList [Category {_catCategory = "Transportation Icons"},Category {_catCategory = "Web Application Icons"}]}

fa_bus :: Icon
fa_bus = Icon {_icoName = IconName {_icnName = "Bus"}, _icoId = IconId {_iidId = "bus"}, _icoChar = Unicode {_uniChar = '\61959'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,2], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "vehicle"}]), _icoCategories = fromList [Category {_catCategory = "Transportation Icons"},Category {_catCategory = "Web Application Icons"}]}

fa_ioxhost :: Icon
fa_ioxhost = Icon {_icoName = IconName {_icnName = "ioxhost"}, _icoId = IconId {_iidId = "ioxhost"}, _icoChar = Unicode {_uniChar = '\61960'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,2], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_angellist :: Icon
fa_angellist = Icon {_icoName = IconName {_icnName = "AngelList"}, _icoId = IconId {_iidId = "angellist"}, _icoChar = Unicode {_uniChar = '\61961'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,2], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_cc :: Icon
fa_cc = Icon {_icoName = IconName {_icnName = "Closed Captions"}, _icoId = IconId {_iidId = "cc"}, _icoChar = Unicode {_uniChar = '\61962'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,2], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Accessibility Icons"},Category {_catCategory = "Web Application Icons"}]}

fa_ils :: Icon
fa_ils = Icon {_icoName = IconName {_icnName = "Shekel (ILS)"}, _icoId = IconId {_iidId = "ils"}, _icoChar = Unicode {_uniChar = '\61963'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,2], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Currency Icons"}]}

fa_meanpath :: Icon
fa_meanpath = Icon {_icoName = IconName {_icnName = "meanpath"}, _icoId = IconId {_iidId = "meanpath"}, _icoChar = Unicode {_uniChar = '\61964'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,2], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_buysellads :: Icon
fa_buysellads = Icon {_icoName = IconName {_icnName = "BuySellAds"}, _icoId = IconId {_iidId = "buysellads"}, _icoChar = Unicode {_uniChar = '\61965'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,3], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_connectdevelop :: Icon
fa_connectdevelop = Icon {_icoName = IconName {_icnName = "Connect Develop"}, _icoId = IconId {_iidId = "connectdevelop"}, _icoChar = Unicode {_uniChar = '\61966'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,3], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_dashcube :: Icon
fa_dashcube = Icon {_icoName = IconName {_icnName = "DashCube"}, _icoId = IconId {_iidId = "dashcube"}, _icoChar = Unicode {_uniChar = '\61968'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,3], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_forumbee :: Icon
fa_forumbee = Icon {_icoName = IconName {_icnName = "Forumbee"}, _icoId = IconId {_iidId = "forumbee"}, _icoChar = Unicode {_uniChar = '\61969'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,3], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_leanpub :: Icon
fa_leanpub = Icon {_icoName = IconName {_icnName = "Leanpub"}, _icoId = IconId {_iidId = "leanpub"}, _icoChar = Unicode {_uniChar = '\61970'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,3], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_sellsy :: Icon
fa_sellsy = Icon {_icoName = IconName {_icnName = "Sellsy"}, _icoId = IconId {_iidId = "sellsy"}, _icoChar = Unicode {_uniChar = '\61971'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,3], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_shirtsinbulk :: Icon
fa_shirtsinbulk = Icon {_icoName = IconName {_icnName = "Shirts in Bulk"}, _icoId = IconId {_iidId = "shirtsinbulk"}, _icoChar = Unicode {_uniChar = '\61972'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,3], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_simplybuilt :: Icon
fa_simplybuilt = Icon {_icoName = IconName {_icnName = "SimplyBuilt"}, _icoId = IconId {_iidId = "simplybuilt"}, _icoChar = Unicode {_uniChar = '\61973'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,3], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_skyatlas :: Icon
fa_skyatlas = Icon {_icoName = IconName {_icnName = "skyatlas"}, _icoId = IconId {_iidId = "skyatlas"}, _icoChar = Unicode {_uniChar = '\61974'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,3], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_cart_plus :: Icon
fa_cart_plus = Icon {_icoName = IconName {_icnName = "Add to Shopping Cart"}, _icoId = IconId {_iidId = "cart-plus"}, _icoChar = Unicode {_uniChar = '\61975'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,3], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "add"},Filter {_filtFilter = "shopping"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_cart_arrow_down :: Icon
fa_cart_arrow_down = Icon {_icoName = IconName {_icnName = "Shopping Cart Arrow Down"}, _icoId = IconId {_iidId = "cart-arrow-down"}, _icoChar = Unicode {_uniChar = '\61976'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,3], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "shopping"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_diamond :: Icon
fa_diamond = Icon {_icoName = IconName {_icnName = "Diamond"}, _icoId = IconId {_iidId = "diamond"}, _icoChar = Unicode {_uniChar = '\61977'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,3], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "gem"},Filter {_filtFilter = "gemstone"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_ship :: Icon
fa_ship = Icon {_icoName = IconName {_icnName = "Ship"}, _icoId = IconId {_iidId = "ship"}, _icoChar = Unicode {_uniChar = '\61978'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,3], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "boat"},Filter {_filtFilter = "sea"}]), _icoCategories = fromList [Category {_catCategory = "Transportation Icons"},Category {_catCategory = "Web Application Icons"}]}

fa_user_secret :: Icon
fa_user_secret = Icon {_icoName = IconName {_icnName = "User Secret"}, _icoId = IconId {_iidId = "user-secret"}, _icoChar = Unicode {_uniChar = '\61979'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,3], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "incognito"},Filter {_filtFilter = "privacy"},Filter {_filtFilter = "spy"},Filter {_filtFilter = "whisper"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_motorcycle :: Icon
fa_motorcycle = Icon {_icoName = IconName {_icnName = "Motorcycle"}, _icoId = IconId {_iidId = "motorcycle"}, _icoChar = Unicode {_uniChar = '\61980'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,3], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "bike"},Filter {_filtFilter = "vehicle"}]), _icoCategories = fromList [Category {_catCategory = "Transportation Icons"},Category {_catCategory = "Web Application Icons"}]}

fa_street_view :: Icon
fa_street_view = Icon {_icoName = IconName {_icnName = "Street View"}, _icoId = IconId {_iidId = "street-view"}, _icoChar = Unicode {_uniChar = '\61981'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,3], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "map"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_heartbeat :: Icon
fa_heartbeat = Icon {_icoName = IconName {_icnName = "Heartbeat"}, _icoId = IconId {_iidId = "heartbeat"}, _icoChar = Unicode {_uniChar = '\61982'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,3], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "ekg"}]), _icoCategories = fromList [Category {_catCategory = "Medical Icons"},Category {_catCategory = "Web Application Icons"}]}

fa_venus :: Icon
fa_venus = Icon {_icoName = IconName {_icnName = "Venus"}, _icoId = IconId {_iidId = "venus"}, _icoChar = Unicode {_uniChar = '\61985'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,3], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "female"}]), _icoCategories = fromList [Category {_catCategory = "Gender Icons"}]}

fa_mars :: Icon
fa_mars = Icon {_icoName = IconName {_icnName = "Mars"}, _icoId = IconId {_iidId = "mars"}, _icoChar = Unicode {_uniChar = '\61986'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,3], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "male"}]), _icoCategories = fromList [Category {_catCategory = "Gender Icons"}]}

fa_mercury :: Icon
fa_mercury = Icon {_icoName = IconName {_icnName = "Mercury"}, _icoId = IconId {_iidId = "mercury"}, _icoChar = Unicode {_uniChar = '\61987'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,3], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "transgender"}]), _icoCategories = fromList [Category {_catCategory = "Gender Icons"}]}

fa_transgender :: Icon
fa_transgender = Icon {_icoName = IconName {_icnName = "Transgender"}, _icoId = IconId {_iidId = "transgender"}, _icoChar = Unicode {_uniChar = '\61988'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,3], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Gender Icons"}]}

fa_transgender_alt :: Icon
fa_transgender_alt = Icon {_icoName = IconName {_icnName = "Transgender Alt"}, _icoId = IconId {_iidId = "transgender-alt"}, _icoChar = Unicode {_uniChar = '\61989'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,3], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Gender Icons"}]}

fa_venus_double :: Icon
fa_venus_double = Icon {_icoName = IconName {_icnName = "Venus Double"}, _icoId = IconId {_iidId = "venus-double"}, _icoChar = Unicode {_uniChar = '\61990'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,3], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Gender Icons"}]}

fa_mars_double :: Icon
fa_mars_double = Icon {_icoName = IconName {_icnName = "Mars Double"}, _icoId = IconId {_iidId = "mars-double"}, _icoChar = Unicode {_uniChar = '\61991'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,3], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Gender Icons"}]}

fa_venus_mars :: Icon
fa_venus_mars = Icon {_icoName = IconName {_icnName = "Venus Mars"}, _icoId = IconId {_iidId = "venus-mars"}, _icoChar = Unicode {_uniChar = '\61992'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,3], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Gender Icons"}]}

fa_mars_stroke :: Icon
fa_mars_stroke = Icon {_icoName = IconName {_icnName = "Mars Stroke"}, _icoId = IconId {_iidId = "mars-stroke"}, _icoChar = Unicode {_uniChar = '\61993'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,3], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Gender Icons"}]}

fa_mars_stroke_v :: Icon
fa_mars_stroke_v = Icon {_icoName = IconName {_icnName = "Mars Stroke Vertical"}, _icoId = IconId {_iidId = "mars-stroke-v"}, _icoChar = Unicode {_uniChar = '\61994'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,3], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Gender Icons"}]}

fa_mars_stroke_h :: Icon
fa_mars_stroke_h = Icon {_icoName = IconName {_icnName = "Mars Stroke Horizontal"}, _icoId = IconId {_iidId = "mars-stroke-h"}, _icoChar = Unicode {_uniChar = '\61995'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,3], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Gender Icons"}]}

fa_neuter :: Icon
fa_neuter = Icon {_icoName = IconName {_icnName = "Neuter"}, _icoId = IconId {_iidId = "neuter"}, _icoChar = Unicode {_uniChar = '\61996'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,3], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Gender Icons"}]}

fa_genderless :: Icon
fa_genderless = Icon {_icoName = IconName {_icnName = "Genderless"}, _icoId = IconId {_iidId = "genderless"}, _icoChar = Unicode {_uniChar = '\61997'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,4], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Gender Icons"}]}

fa_facebook_official :: Icon
fa_facebook_official = Icon {_icoName = IconName {_icnName = "Facebook Official"}, _icoId = IconId {_iidId = "facebook-official"}, _icoChar = Unicode {_uniChar = '\62000'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,3], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_pinterest_p :: Icon
fa_pinterest_p = Icon {_icoName = IconName {_icnName = "Pinterest P"}, _icoId = IconId {_iidId = "pinterest-p"}, _icoChar = Unicode {_uniChar = '\62001'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,3], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_whatsapp :: Icon
fa_whatsapp = Icon {_icoName = IconName {_icnName = "What's App"}, _icoId = IconId {_iidId = "whatsapp"}, _icoChar = Unicode {_uniChar = '\62002'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,3], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_server :: Icon
fa_server = Icon {_icoName = IconName {_icnName = "Server"}, _icoId = IconId {_iidId = "server"}, _icoChar = Unicode {_uniChar = '\62003'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,3], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_user_plus :: Icon
fa_user_plus = Icon {_icoName = IconName {_icnName = "Add User"}, _icoId = IconId {_iidId = "user-plus"}, _icoChar = Unicode {_uniChar = '\62004'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,3], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "sign up"},Filter {_filtFilter = "signup"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_user_times :: Icon
fa_user_times = Icon {_icoName = IconName {_icnName = "Remove User"}, _icoId = IconId {_iidId = "user-times"}, _icoChar = Unicode {_uniChar = '\62005'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,3], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_bed :: Icon
fa_bed = Icon {_icoName = IconName {_icnName = "Bed"}, _icoId = IconId {_iidId = "bed"}, _icoChar = Unicode {_uniChar = '\62006'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,3], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "travel"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_viacoin :: Icon
fa_viacoin = Icon {_icoName = IconName {_icnName = "Viacoin"}, _icoId = IconId {_iidId = "viacoin"}, _icoChar = Unicode {_uniChar = '\62007'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,3], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_train :: Icon
fa_train = Icon {_icoName = IconName {_icnName = "Train"}, _icoId = IconId {_iidId = "train"}, _icoChar = Unicode {_uniChar = '\62008'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,3], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Transportation Icons"}]}

fa_subway :: Icon
fa_subway = Icon {_icoName = IconName {_icnName = "Subway"}, _icoId = IconId {_iidId = "subway"}, _icoChar = Unicode {_uniChar = '\62009'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,3], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Transportation Icons"}]}

fa_medium :: Icon
fa_medium = Icon {_icoName = IconName {_icnName = "Medium"}, _icoId = IconId {_iidId = "medium"}, _icoChar = Unicode {_uniChar = '\62010'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,3], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_y_combinator :: Icon
fa_y_combinator = Icon {_icoName = IconName {_icnName = "Y Combinator"}, _icoId = IconId {_iidId = "y-combinator"}, _icoChar = Unicode {_uniChar = '\62011'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,4], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_optin_monster :: Icon
fa_optin_monster = Icon {_icoName = IconName {_icnName = "Optin Monster"}, _icoId = IconId {_iidId = "optin-monster"}, _icoChar = Unicode {_uniChar = '\62012'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,4], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_opencart :: Icon
fa_opencart = Icon {_icoName = IconName {_icnName = "OpenCart"}, _icoId = IconId {_iidId = "opencart"}, _icoChar = Unicode {_uniChar = '\62013'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,4], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_expeditedssl :: Icon
fa_expeditedssl = Icon {_icoName = IconName {_icnName = "ExpeditedSSL"}, _icoId = IconId {_iidId = "expeditedssl"}, _icoChar = Unicode {_uniChar = '\62014'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,4], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_battery_full :: Icon
fa_battery_full = Icon {_icoName = IconName {_icnName = "Battery Full"}, _icoId = IconId {_iidId = "battery-full"}, _icoChar = Unicode {_uniChar = '\62016'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,4], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "power"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_battery_three_quarters :: Icon
fa_battery_three_quarters = Icon {_icoName = IconName {_icnName = "Battery 3/4 Full"}, _icoId = IconId {_iidId = "battery-three-quarters"}, _icoChar = Unicode {_uniChar = '\62017'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,4], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "power"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_battery_half :: Icon
fa_battery_half = Icon {_icoName = IconName {_icnName = "Battery 1/2 Full"}, _icoId = IconId {_iidId = "battery-half"}, _icoChar = Unicode {_uniChar = '\62018'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,4], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "power"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_battery_quarter :: Icon
fa_battery_quarter = Icon {_icoName = IconName {_icnName = "Battery 1/4 Full"}, _icoId = IconId {_iidId = "battery-quarter"}, _icoChar = Unicode {_uniChar = '\62019'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,4], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "power"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_battery_empty :: Icon
fa_battery_empty = Icon {_icoName = IconName {_icnName = "Battery Empty"}, _icoId = IconId {_iidId = "battery-empty"}, _icoChar = Unicode {_uniChar = '\62020'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,4], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "power"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_mouse_pointer :: Icon
fa_mouse_pointer = Icon {_icoName = IconName {_icnName = "Mouse Pointer"}, _icoId = IconId {_iidId = "mouse-pointer"}, _icoChar = Unicode {_uniChar = '\62021'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,4], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_i_cursor :: Icon
fa_i_cursor = Icon {_icoName = IconName {_icnName = "I Beam Cursor"}, _icoId = IconId {_iidId = "i-cursor"}, _icoChar = Unicode {_uniChar = '\62022'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,4], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_object_group :: Icon
fa_object_group = Icon {_icoName = IconName {_icnName = "Object Group"}, _icoId = IconId {_iidId = "object-group"}, _icoChar = Unicode {_uniChar = '\62023'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,4], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_object_ungroup :: Icon
fa_object_ungroup = Icon {_icoName = IconName {_icnName = "Object Ungroup"}, _icoId = IconId {_iidId = "object-ungroup"}, _icoChar = Unicode {_uniChar = '\62024'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,4], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_sticky_note :: Icon
fa_sticky_note = Icon {_icoName = IconName {_icnName = "Sticky Note"}, _icoId = IconId {_iidId = "sticky-note"}, _icoChar = Unicode {_uniChar = '\62025'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,4], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_sticky_note_o :: Icon
fa_sticky_note_o = Icon {_icoName = IconName {_icnName = "Sticky Note Outlined"}, _icoId = IconId {_iidId = "sticky-note-o"}, _icoChar = Unicode {_uniChar = '\62026'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,4], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_cc_jcb :: Icon
fa_cc_jcb = Icon {_icoName = IconName {_icnName = "JCB Credit Card"}, _icoId = IconId {_iidId = "cc-jcb"}, _icoChar = Unicode {_uniChar = '\62027'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,4], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Brand Icons"},Category {_catCategory = "Payment Icons"}]}

fa_cc_diners_club :: Icon
fa_cc_diners_club = Icon {_icoName = IconName {_icnName = "Diner's Club Credit Card"}, _icoId = IconId {_iidId = "cc-diners-club"}, _icoChar = Unicode {_uniChar = '\62028'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,4], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Brand Icons"},Category {_catCategory = "Payment Icons"}]}

fa_clone :: Icon
fa_clone = Icon {_icoName = IconName {_icnName = "Clone"}, _icoId = IconId {_iidId = "clone"}, _icoChar = Unicode {_uniChar = '\62029'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,4], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "copy"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_balance_scale :: Icon
fa_balance_scale = Icon {_icoName = IconName {_icnName = "Balance Scale"}, _icoId = IconId {_iidId = "balance-scale"}, _icoChar = Unicode {_uniChar = '\62030'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,4], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_hourglass_o :: Icon
fa_hourglass_o = Icon {_icoName = IconName {_icnName = "Hourglass Outlined"}, _icoId = IconId {_iidId = "hourglass-o"}, _icoChar = Unicode {_uniChar = '\62032'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,4], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_hourglass_start :: Icon
fa_hourglass_start = Icon {_icoName = IconName {_icnName = "Hourglass Start"}, _icoId = IconId {_iidId = "hourglass-start"}, _icoChar = Unicode {_uniChar = '\62033'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,4], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_hourglass_half :: Icon
fa_hourglass_half = Icon {_icoName = IconName {_icnName = "Hourglass Half"}, _icoId = IconId {_iidId = "hourglass-half"}, _icoChar = Unicode {_uniChar = '\62034'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,4], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_hourglass_end :: Icon
fa_hourglass_end = Icon {_icoName = IconName {_icnName = "Hourglass End"}, _icoId = IconId {_iidId = "hourglass-end"}, _icoChar = Unicode {_uniChar = '\62035'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,4], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_hourglass :: Icon
fa_hourglass = Icon {_icoName = IconName {_icnName = "Hourglass"}, _icoId = IconId {_iidId = "hourglass"}, _icoChar = Unicode {_uniChar = '\62036'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,4], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_hand_rock_o :: Icon
fa_hand_rock_o = Icon {_icoName = IconName {_icnName = "Rock (Hand)"}, _icoId = IconId {_iidId = "hand-rock-o"}, _icoChar = Unicode {_uniChar = '\62037'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,4], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Hand Icons"},Category {_catCategory = "Web Application Icons"}]}

fa_hand_paper_o :: Icon
fa_hand_paper_o = Icon {_icoName = IconName {_icnName = "Paper (Hand)"}, _icoId = IconId {_iidId = "hand-paper-o"}, _icoChar = Unicode {_uniChar = '\62038'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,4], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "stop"}]), _icoCategories = fromList [Category {_catCategory = "Hand Icons"},Category {_catCategory = "Web Application Icons"}]}

fa_hand_scissors_o :: Icon
fa_hand_scissors_o = Icon {_icoName = IconName {_icnName = "Scissors (Hand)"}, _icoId = IconId {_iidId = "hand-scissors-o"}, _icoChar = Unicode {_uniChar = '\62039'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,4], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Hand Icons"},Category {_catCategory = "Web Application Icons"}]}

fa_hand_lizard_o :: Icon
fa_hand_lizard_o = Icon {_icoName = IconName {_icnName = "Lizard (Hand)"}, _icoId = IconId {_iidId = "hand-lizard-o"}, _icoChar = Unicode {_uniChar = '\62040'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,4], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Hand Icons"},Category {_catCategory = "Web Application Icons"}]}

fa_hand_spock_o :: Icon
fa_hand_spock_o = Icon {_icoName = IconName {_icnName = "Spock (Hand)"}, _icoId = IconId {_iidId = "hand-spock-o"}, _icoChar = Unicode {_uniChar = '\62041'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,4], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Hand Icons"},Category {_catCategory = "Web Application Icons"}]}

fa_hand_pointer_o :: Icon
fa_hand_pointer_o = Icon {_icoName = IconName {_icnName = "Hand Pointer"}, _icoId = IconId {_iidId = "hand-pointer-o"}, _icoChar = Unicode {_uniChar = '\62042'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,4], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Hand Icons"},Category {_catCategory = "Web Application Icons"}]}

fa_hand_peace_o :: Icon
fa_hand_peace_o = Icon {_icoName = IconName {_icnName = "Hand Peace"}, _icoId = IconId {_iidId = "hand-peace-o"}, _icoChar = Unicode {_uniChar = '\62043'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,4], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Hand Icons"},Category {_catCategory = "Web Application Icons"}]}

fa_trademark :: Icon
fa_trademark = Icon {_icoName = IconName {_icnName = "Trademark"}, _icoId = IconId {_iidId = "trademark"}, _icoChar = Unicode {_uniChar = '\62044'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,4], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_registered :: Icon
fa_registered = Icon {_icoName = IconName {_icnName = "Registered Trademark"}, _icoId = IconId {_iidId = "registered"}, _icoChar = Unicode {_uniChar = '\62045'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,4], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_creative_commons :: Icon
fa_creative_commons = Icon {_icoName = IconName {_icnName = "Creative Commons"}, _icoId = IconId {_iidId = "creative-commons"}, _icoChar = Unicode {_uniChar = '\62046'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,4], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_gg :: Icon
fa_gg = Icon {_icoName = IconName {_icnName = "GG Currency"}, _icoId = IconId {_iidId = "gg"}, _icoChar = Unicode {_uniChar = '\62048'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,4], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Brand Icons"},Category {_catCategory = "Currency Icons"}]}

fa_gg_circle :: Icon
fa_gg_circle = Icon {_icoName = IconName {_icnName = "GG Currency Circle"}, _icoId = IconId {_iidId = "gg-circle"}, _icoChar = Unicode {_uniChar = '\62049'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,4], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Brand Icons"},Category {_catCategory = "Currency Icons"}]}

fa_tripadvisor :: Icon
fa_tripadvisor = Icon {_icoName = IconName {_icnName = "TripAdvisor"}, _icoId = IconId {_iidId = "tripadvisor"}, _icoChar = Unicode {_uniChar = '\62050'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,4], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_odnoklassniki :: Icon
fa_odnoklassniki = Icon {_icoName = IconName {_icnName = "Odnoklassniki"}, _icoId = IconId {_iidId = "odnoklassniki"}, _icoChar = Unicode {_uniChar = '\62051'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,4], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_odnoklassniki_square :: Icon
fa_odnoklassniki_square = Icon {_icoName = IconName {_icnName = "Odnoklassniki Square"}, _icoId = IconId {_iidId = "odnoklassniki-square"}, _icoChar = Unicode {_uniChar = '\62052'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,4], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_get_pocket :: Icon
fa_get_pocket = Icon {_icoName = IconName {_icnName = "Get Pocket"}, _icoId = IconId {_iidId = "get-pocket"}, _icoChar = Unicode {_uniChar = '\62053'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,4], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_wikipedia_w :: Icon
fa_wikipedia_w = Icon {_icoName = IconName {_icnName = "Wikipedia W"}, _icoId = IconId {_iidId = "wikipedia-w"}, _icoChar = Unicode {_uniChar = '\62054'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,4], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_safari :: Icon
fa_safari = Icon {_icoName = IconName {_icnName = "Safari"}, _icoId = IconId {_iidId = "safari"}, _icoChar = Unicode {_uniChar = '\62055'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,4], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "browser"}]), _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_chrome :: Icon
fa_chrome = Icon {_icoName = IconName {_icnName = "Chrome"}, _icoId = IconId {_iidId = "chrome"}, _icoChar = Unicode {_uniChar = '\62056'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,4], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "browser"}]), _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_firefox :: Icon
fa_firefox = Icon {_icoName = IconName {_icnName = "Firefox"}, _icoId = IconId {_iidId = "firefox"}, _icoChar = Unicode {_uniChar = '\62057'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,4], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "browser"}]), _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_opera :: Icon
fa_opera = Icon {_icoName = IconName {_icnName = "Opera"}, _icoId = IconId {_iidId = "opera"}, _icoChar = Unicode {_uniChar = '\62058'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,4], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_internet_explorer :: Icon
fa_internet_explorer = Icon {_icoName = IconName {_icnName = "Internet-explorer"}, _icoId = IconId {_iidId = "internet-explorer"}, _icoChar = Unicode {_uniChar = '\62059'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,4], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "browser"},Filter {_filtFilter = "ie"}]), _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_television :: Icon
fa_television = Icon {_icoName = IconName {_icnName = "Television"}, _icoId = IconId {_iidId = "television"}, _icoChar = Unicode {_uniChar = '\62060'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,4], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "computer"},Filter {_filtFilter = "display"},Filter {_filtFilter = "monitor"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_contao :: Icon
fa_contao = Icon {_icoName = IconName {_icnName = "Contao"}, _icoId = IconId {_iidId = "contao"}, _icoChar = Unicode {_uniChar = '\62061'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,4], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_500px :: Icon
fa_500px = Icon {_icoName = IconName {_icnName = "500px"}, _icoId = IconId {_iidId = "500px"}, _icoChar = Unicode {_uniChar = '\62062'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,4], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_amazon :: Icon
fa_amazon = Icon {_icoName = IconName {_icnName = "Amazon"}, _icoId = IconId {_iidId = "amazon"}, _icoChar = Unicode {_uniChar = '\62064'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,4], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_calendar_plus_o :: Icon
fa_calendar_plus_o = Icon {_icoName = IconName {_icnName = "Calendar Plus Outlined"}, _icoId = IconId {_iidId = "calendar-plus-o"}, _icoChar = Unicode {_uniChar = '\62065'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,4], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_calendar_minus_o :: Icon
fa_calendar_minus_o = Icon {_icoName = IconName {_icnName = "Calendar Minus Outlined"}, _icoId = IconId {_iidId = "calendar-minus-o"}, _icoChar = Unicode {_uniChar = '\62066'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,4], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_calendar_times_o :: Icon
fa_calendar_times_o = Icon {_icoName = IconName {_icnName = "Calendar Times Outlined"}, _icoId = IconId {_iidId = "calendar-times-o"}, _icoChar = Unicode {_uniChar = '\62067'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,4], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_calendar_check_o :: Icon
fa_calendar_check_o = Icon {_icoName = IconName {_icnName = "Calendar Check Outlined"}, _icoId = IconId {_iidId = "calendar-check-o"}, _icoChar = Unicode {_uniChar = '\62068'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,4], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "ok"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_industry :: Icon
fa_industry = Icon {_icoName = IconName {_icnName = "Industry"}, _icoId = IconId {_iidId = "industry"}, _icoChar = Unicode {_uniChar = '\62069'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,4], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "factory"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_map_pin :: Icon
fa_map_pin = Icon {_icoName = IconName {_icnName = "Map Pin"}, _icoId = IconId {_iidId = "map-pin"}, _icoChar = Unicode {_uniChar = '\62070'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,4], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_map_signs :: Icon
fa_map_signs = Icon {_icoName = IconName {_icnName = "Map Signs"}, _icoId = IconId {_iidId = "map-signs"}, _icoChar = Unicode {_uniChar = '\62071'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,4], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_map_o :: Icon
fa_map_o = Icon {_icoName = IconName {_icnName = "Map Outline"}, _icoId = IconId {_iidId = "map-o"}, _icoChar = Unicode {_uniChar = '\62072'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,4], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_map :: Icon
fa_map = Icon {_icoName = IconName {_icnName = "Map"}, _icoId = IconId {_iidId = "map"}, _icoChar = Unicode {_uniChar = '\62073'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,4], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_commenting :: Icon
fa_commenting = Icon {_icoName = IconName {_icnName = "Commenting"}, _icoId = IconId {_iidId = "commenting"}, _icoChar = Unicode {_uniChar = '\62074'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,4], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "message"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_commenting_o :: Icon
fa_commenting_o = Icon {_icoName = IconName {_icnName = "Commenting Outlined"}, _icoId = IconId {_iidId = "commenting-o"}, _icoChar = Unicode {_uniChar = '\62075'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,4], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "message"}]), _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_houzz :: Icon
fa_houzz = Icon {_icoName = IconName {_icnName = "Houzz"}, _icoId = IconId {_iidId = "houzz"}, _icoChar = Unicode {_uniChar = '\62076'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,4], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_vimeo :: Icon
fa_vimeo = Icon {_icoName = IconName {_icnName = "Vimeo"}, _icoId = IconId {_iidId = "vimeo"}, _icoChar = Unicode {_uniChar = '\62077'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,4], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_black_tie :: Icon
fa_black_tie = Icon {_icoName = IconName {_icnName = "Font Awesome Black Tie"}, _icoId = IconId {_iidId = "black-tie"}, _icoChar = Unicode {_uniChar = '\62078'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,4], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_fonticons :: Icon
fa_fonticons = Icon {_icoName = IconName {_icnName = "Fonticons"}, _icoId = IconId {_iidId = "fonticons"}, _icoChar = Unicode {_uniChar = '\62080'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,4], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_reddit_alien :: Icon
fa_reddit_alien = Icon {_icoName = IconName {_icnName = "reddit Alien"}, _icoId = IconId {_iidId = "reddit-alien"}, _icoChar = Unicode {_uniChar = '\62081'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,5], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_edge :: Icon
fa_edge = Icon {_icoName = IconName {_icnName = "Edge Browser"}, _icoId = IconId {_iidId = "edge"}, _icoChar = Unicode {_uniChar = '\62082'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,5], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "browser"},Filter {_filtFilter = "ie"}]), _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_credit_card_alt :: Icon
fa_credit_card_alt = Icon {_icoName = IconName {_icnName = "Credit Card"}, _icoId = IconId {_iidId = "credit-card-alt"}, _icoChar = Unicode {_uniChar = '\62083'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,5], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "buy"},Filter {_filtFilter = "checkout"},Filter {_filtFilter = "credit card"},Filter {_filtFilter = "debit"},Filter {_filtFilter = "money"},Filter {_filtFilter = "payment"},Filter {_filtFilter = "purchase"}]), _icoCategories = fromList [Category {_catCategory = "Payment Icons"},Category {_catCategory = "Web Application Icons"}]}

fa_codiepie :: Icon
fa_codiepie = Icon {_icoName = IconName {_icnName = "Codie Pie"}, _icoId = IconId {_iidId = "codiepie"}, _icoChar = Unicode {_uniChar = '\62084'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,5], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_modx :: Icon
fa_modx = Icon {_icoName = IconName {_icnName = "MODX"}, _icoId = IconId {_iidId = "modx"}, _icoChar = Unicode {_uniChar = '\62085'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,5], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_fort_awesome :: Icon
fa_fort_awesome = Icon {_icoName = IconName {_icnName = "Fort Awesome"}, _icoId = IconId {_iidId = "fort-awesome"}, _icoChar = Unicode {_uniChar = '\62086'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,5], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_usb :: Icon
fa_usb = Icon {_icoName = IconName {_icnName = "USB"}, _icoId = IconId {_iidId = "usb"}, _icoChar = Unicode {_uniChar = '\62087'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,5], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_product_hunt :: Icon
fa_product_hunt = Icon {_icoName = IconName {_icnName = "Product Hunt"}, _icoId = IconId {_iidId = "product-hunt"}, _icoChar = Unicode {_uniChar = '\62088'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,5], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_mixcloud :: Icon
fa_mixcloud = Icon {_icoName = IconName {_icnName = "Mixcloud"}, _icoId = IconId {_iidId = "mixcloud"}, _icoChar = Unicode {_uniChar = '\62089'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,5], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_scribd :: Icon
fa_scribd = Icon {_icoName = IconName {_icnName = "Scribd"}, _icoId = IconId {_iidId = "scribd"}, _icoChar = Unicode {_uniChar = '\62090'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,5], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_pause_circle :: Icon
fa_pause_circle = Icon {_icoName = IconName {_icnName = "Pause Circle"}, _icoId = IconId {_iidId = "pause-circle"}, _icoChar = Unicode {_uniChar = '\62091'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,5], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Video Player Icons"}]}

fa_pause_circle_o :: Icon
fa_pause_circle_o = Icon {_icoName = IconName {_icnName = "Pause Circle Outlined"}, _icoId = IconId {_iidId = "pause-circle-o"}, _icoChar = Unicode {_uniChar = '\62092'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,5], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Video Player Icons"}]}

fa_stop_circle :: Icon
fa_stop_circle = Icon {_icoName = IconName {_icnName = "Stop Circle"}, _icoId = IconId {_iidId = "stop-circle"}, _icoChar = Unicode {_uniChar = '\62093'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,5], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Video Player Icons"}]}

fa_stop_circle_o :: Icon
fa_stop_circle_o = Icon {_icoName = IconName {_icnName = "Stop Circle Outlined"}, _icoId = IconId {_iidId = "stop-circle-o"}, _icoChar = Unicode {_uniChar = '\62094'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,5], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Video Player Icons"}]}

fa_shopping_bag :: Icon
fa_shopping_bag = Icon {_icoName = IconName {_icnName = "Shopping Bag"}, _icoId = IconId {_iidId = "shopping-bag"}, _icoChar = Unicode {_uniChar = '\62096'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,5], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_shopping_basket :: Icon
fa_shopping_basket = Icon {_icoName = IconName {_icnName = "Shopping Basket"}, _icoId = IconId {_iidId = "shopping-basket"}, _icoChar = Unicode {_uniChar = '\62097'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,5], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_hashtag :: Icon
fa_hashtag = Icon {_icoName = IconName {_icnName = "Hashtag"}, _icoId = IconId {_iidId = "hashtag"}, _icoChar = Unicode {_uniChar = '\62098'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,5], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_bluetooth :: Icon
fa_bluetooth = Icon {_icoName = IconName {_icnName = "Bluetooth"}, _icoId = IconId {_iidId = "bluetooth"}, _icoChar = Unicode {_uniChar = '\62099'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,5], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Brand Icons"},Category {_catCategory = "Web Application Icons"}]}

fa_bluetooth_b :: Icon
fa_bluetooth_b = Icon {_icoName = IconName {_icnName = "Bluetooth"}, _icoId = IconId {_iidId = "bluetooth-b"}, _icoChar = Unicode {_uniChar = '\62100'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,5], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Brand Icons"},Category {_catCategory = "Web Application Icons"}]}

fa_percent :: Icon
fa_percent = Icon {_icoName = IconName {_icnName = "Percent"}, _icoId = IconId {_iidId = "percent"}, _icoChar = Unicode {_uniChar = '\62101'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,5], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Web Application Icons"}]}

fa_gitlab :: Icon
fa_gitlab = Icon {_icoName = IconName {_icnName = "GitLab"}, _icoId = IconId {_iidId = "gitlab"}, _icoChar = Unicode {_uniChar = '\62102'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,6], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_wpbeginner :: Icon
fa_wpbeginner = Icon {_icoName = IconName {_icnName = "WPBeginner"}, _icoId = IconId {_iidId = "wpbeginner"}, _icoChar = Unicode {_uniChar = '\62103'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,6], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_wpforms :: Icon
fa_wpforms = Icon {_icoName = IconName {_icnName = "WPForms"}, _icoId = IconId {_iidId = "wpforms"}, _icoChar = Unicode {_uniChar = '\62104'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,6], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_envira :: Icon
fa_envira = Icon {_icoName = IconName {_icnName = "Envira Gallery"}, _icoId = IconId {_iidId = "envira"}, _icoChar = Unicode {_uniChar = '\62105'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,6], versionTags = []}}, _icoFilters = Just (fromList [Filter {_filtFilter = "leaf"}]), _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_universal_access :: Icon
fa_universal_access = Icon {_icoName = IconName {_icnName = "Universal Access"}, _icoId = IconId {_iidId = "universal-access"}, _icoChar = Unicode {_uniChar = '\62106'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,6], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Accessibility Icons"},Category {_catCategory = "Web Application Icons"}]}

fa_wheelchair_alt :: Icon
fa_wheelchair_alt = Icon {_icoName = IconName {_icnName = "Wheelchair Alt"}, _icoId = IconId {_iidId = "wheelchair-alt"}, _icoChar = Unicode {_uniChar = '\62107'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,6], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Accessibility Icons"},Category {_catCategory = "Web Application Icons"}]}

fa_question_circle_o :: Icon
fa_question_circle_o = Icon {_icoName = IconName {_icnName = "Question Circle Outlined"}, _icoId = IconId {_iidId = "question-circle-o"}, _icoChar = Unicode {_uniChar = '\62108'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,6], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Accessibility Icons"},Category {_catCategory = "Web Application Icons"}]}

fa_blind :: Icon
fa_blind = Icon {_icoName = IconName {_icnName = "Blind"}, _icoId = IconId {_iidId = "blind"}, _icoChar = Unicode {_uniChar = '\62109'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,6], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Accessibility Icons"},Category {_catCategory = "Web Application Icons"}]}

fa_audio_description :: Icon
fa_audio_description = Icon {_icoName = IconName {_icnName = "Audio Description"}, _icoId = IconId {_iidId = "audio-description"}, _icoChar = Unicode {_uniChar = '\62110'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,6], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Accessibility Icons"},Category {_catCategory = "Web Application Icons"}]}

fa_volume_control_phone :: Icon
fa_volume_control_phone = Icon {_icoName = IconName {_icnName = "Volume Control Phone"}, _icoId = IconId {_iidId = "volume-control-phone"}, _icoChar = Unicode {_uniChar = '\62112'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,6], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Accessibility Icons"},Category {_catCategory = "Web Application Icons"}]}

fa_braille :: Icon
fa_braille = Icon {_icoName = IconName {_icnName = "Braille"}, _icoId = IconId {_iidId = "braille"}, _icoChar = Unicode {_uniChar = '\62113'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,6], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Accessibility Icons"},Category {_catCategory = "Web Application Icons"}]}

fa_assistive_listening_systems :: Icon
fa_assistive_listening_systems = Icon {_icoName = IconName {_icnName = "Assistive Listening Systems"}, _icoId = IconId {_iidId = "assistive-listening-systems"}, _icoChar = Unicode {_uniChar = '\62114'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,6], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Accessibility Icons"},Category {_catCategory = "Web Application Icons"}]}

fa_american_sign_language_interpreting :: Icon
fa_american_sign_language_interpreting = Icon {_icoName = IconName {_icnName = "American Sign Language Interpreting"}, _icoId = IconId {_iidId = "american-sign-language-interpreting"}, _icoChar = Unicode {_uniChar = '\62115'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,6], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Accessibility Icons"},Category {_catCategory = "Web Application Icons"}]}

fa_deaf :: Icon
fa_deaf = Icon {_icoName = IconName {_icnName = "Deaf"}, _icoId = IconId {_iidId = "deaf"}, _icoChar = Unicode {_uniChar = '\62116'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,6], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Accessibility Icons"},Category {_catCategory = "Web Application Icons"}]}

fa_glide :: Icon
fa_glide = Icon {_icoName = IconName {_icnName = "Glide"}, _icoId = IconId {_iidId = "glide"}, _icoChar = Unicode {_uniChar = '\62117'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,6], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_glide_g :: Icon
fa_glide_g = Icon {_icoName = IconName {_icnName = "Glide G"}, _icoId = IconId {_iidId = "glide-g"}, _icoChar = Unicode {_uniChar = '\62118'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,6], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_sign_language :: Icon
fa_sign_language = Icon {_icoName = IconName {_icnName = "Sign Language"}, _icoId = IconId {_iidId = "sign-language"}, _icoChar = Unicode {_uniChar = '\62119'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,6], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Accessibility Icons"},Category {_catCategory = "Web Application Icons"}]}

fa_low_vision :: Icon
fa_low_vision = Icon {_icoName = IconName {_icnName = "Low Vision"}, _icoId = IconId {_iidId = "low-vision"}, _icoChar = Unicode {_uniChar = '\62120'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,6], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Accessibility Icons"},Category {_catCategory = "Web Application Icons"}]}

fa_viadeo :: Icon
fa_viadeo = Icon {_icoName = IconName {_icnName = "Viadeo"}, _icoId = IconId {_iidId = "viadeo"}, _icoChar = Unicode {_uniChar = '\62121'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,6], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_viadeo_square :: Icon
fa_viadeo_square = Icon {_icoName = IconName {_icnName = "Viadeo Square"}, _icoId = IconId {_iidId = "viadeo-square"}, _icoChar = Unicode {_uniChar = '\62122'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,6], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_snapchat :: Icon
fa_snapchat = Icon {_icoName = IconName {_icnName = "Snapchat"}, _icoId = IconId {_iidId = "snapchat"}, _icoChar = Unicode {_uniChar = '\62123'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,6], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_snapchat_ghost :: Icon
fa_snapchat_ghost = Icon {_icoName = IconName {_icnName = "Snapchat Ghost"}, _icoId = IconId {_iidId = "snapchat-ghost"}, _icoChar = Unicode {_uniChar = '\62124'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,6], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

fa_snapchat_square :: Icon
fa_snapchat_square = Icon {_icoName = IconName {_icnName = "Snapchat Square"}, _icoId = IconId {_iidId = "snapchat-square"}, _icoChar = Unicode {_uniChar = '\62125'}, _icoVersion = FAVersion {_vVersion = Version {versionBranch = [4,6], versionTags = []}}, _icoFilters = Nothing, _icoCategories = fromList [Category {_catCategory = "Brand Icons"}]}

