{-# Language TemplateHaskell #-}

module Web.FontAwesome.Embed where

import Control.Lens
import qualified Data.Text as T
import Data.Text (Text)
import Data.Text.Lazy (fromStrict)
import Web.FontAwesome.Types
import Web.FontAwesome.Modifiers
import HSP
import HSP.Monad
import qualified Data.Set as S

-- | Embeddable FontAwesomeIcon
data FontAwesomeIcon = FontAwesomeIcon {
    -- | The actual FontAwesome 'Icon'.
    _faiIcon :: Icon,
    -- | The HTML tag we're using as a wrapper.
    _faiElement :: Text,
    -- | Extra classes to add
    _faiClasses :: FAClasses,
    -- | FontAwesome 'Modifiers'
    _faiModifiers :: Modifiers
    } deriving (Show, Eq, Ord)
makeLenses ''FontAwesomeIcon

-- | Construct a default FontAwesome icon.
--
-- There are no additional classes or modifiers, and the enclosing
-- HTML element is <i></i>.
fontAwesomeIcon :: Icon -> FAClasses -> FontAwesomeIcon
fontAwesomeIcon icon classes =
    FontAwesomeIcon icon "i" classes defaultModifiers

fontAwesomeXML :: FontAwesomeIcon -> XML
fontAwesomeXML icon =
    let
        baseClasses = ["fa", T.intercalate "-" ["fa", icon ^. faiIcon ^. icoId ^. _IconId]]
        modClasses = S.toList . modifiersToText $ (icon ^. faiModifiers)
        addlClasses = S.toList . S.map (view _FAClass) $ (icon ^. faiClasses)

        classes = fromStrict . T.unwords . concat $ [baseClasses, modClasses, addlClasses]
        tag = (Nothing, fromStrict (icon ^. faiElement))
        classAttr = MkAttr ((Nothing, "class"), attrVal classes)
    in
        Element tag [classAttr]  []

instance (XMLGen m, Functor m, Monad m) => EmbedAsChild (HSPT XML m) FontAwesomeIcon where
    asChild = asChild . fontAwesomeXML
